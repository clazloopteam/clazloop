﻿using System;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve (AllMembers = true)]
	public class WebServiceResult
	{
		public string messageError {get; set;}
		public string service { get; set;}
		public string data {get; set;}
	}
}

