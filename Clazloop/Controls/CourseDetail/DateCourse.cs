﻿using System;
using Xamarin.Forms;


namespace Clazloop.Controls.CourseDetail
{
	public class DateCourse : ContentView
	{
		private StackLayout _skDateCourse;
		public Label lblDateCourse;

		public DateCourse (string duration)
		{
			lblDateCourse = new Label () { 
				Style = AppStyle.AuthorTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				VerticalOptions = LayoutOptions.Center,
				Text = duration
			}; 

			_skDateCourse = new StackLayout () {
				Padding = new Thickness(5),
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = {lblDateCourse}
			};
			this.Content = _skDateCourse;
		}
	}
}
