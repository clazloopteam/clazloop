﻿using System;
using Xamarin.Forms;

namespace Clazloop.Controls.CourseDetail
{
	public class LevelCourse : ContentView
	{
		private StackLayout _skLevel;
		public Label lblCourseLevel;

		public LevelCourse (string level)
		{
			lblCourseLevel = new Label () { 
				Style = AppStyle.DurationTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),	
				VerticalOptions = LayoutOptions.Center,
				Text = level
			};

			var imgCourseLevel = new Image () {
				Source = AppStyle.Icons.Level,
				HeightRequest = 12,
				WidthRequest = 12
			};

			_skLevel = new StackLayout (){ 
				Padding = new Thickness(0,0,0,0),
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = {imgCourseLevel, lblCourseLevel}
			};
			this.Content = _skLevel;
		}
	}
}

