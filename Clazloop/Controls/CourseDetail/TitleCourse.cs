﻿using System;
using Xamarin.Forms;

namespace Clazloop.Controls.CourseDetail
{
	public class TitleCourse : ContentView
	{
		private StackLayout _skTitle;
		public Label lblTitle;

		public TitleCourse (string title)
		{
			lblTitle = new Label () { 
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkBlue,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				VerticalOptions = LayoutOptions.Center,
				Text = title
			}; 

			_skTitle = new StackLayout () {
				Padding = new Thickness(5,3),
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = {lblTitle}
			};
			this.Content = _skTitle;
		}
	}
}

