﻿using System;
using Xamarin.Forms;
using System.Collections;
using Clazloop.ListViewTemplates;
using Clazloop.DTOs;

namespace Clazloop.Controls.CourseDetail
{
	public class SuggestionSection : ContentView
	{
		private StackLayout _skSection;

		private Label lblTitle;

		private bool _isExpand;
		public bool IsExpand
		{
			get { return _isExpand; }
			set { _isExpand = value; }
		}

		private string _title; 
		public string Title 
		{
			get { return _title; }
			set { _title = value; }
		}

		private Color _backgroundExpandColor;
		public Color BackgroundExpandColor
		{
			get { return _backgroundExpandColor; }
			set { 
				_skSection.BackgroundColor = value;
				_backgroundExpandColor = value;
			}
		}

		private Color _backgroundContractColor;
		public Color BackgroundContractColor
		{
			get { return _backgroundContractColor; }
			set { 
				_skSection.BackgroundColor = value;
				_backgroundContractColor = value;
			}
		}

		private Color _titleFontColor;
		public Color TitleFontColor
		{
			get { return _titleFontColor; }
			set { 
				_skSection.BackgroundColor = value;
				_titleFontColor = value;
			}
		}

		private IEnumerable _data;
		public IEnumerable ItemSource 
		{
			get { return _data; }
			set { _data = value; }
		}

		public event EventHandler<SectionDTO> SelectedSectionChanged;

		public SuggestionSection (string Title, bool isExpand, IEnumerable Data, Color ExpandBackgroundColor, Color ContractBackgroundColor)
		{
			this.Title = Title;
			this.IsExpand = isExpand;
			this.ItemSource = Data;
			this._backgroundExpandColor = ExpandBackgroundColor;
			this._backgroundContractColor = ContractBackgroundColor;
			_skSection = new StackLayout {
				Padding = 5,
				Spacing = 1,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				BackgroundColor = !_isExpand ? ContractBackgroundColor: ExpandBackgroundColor,
			};

			var imgTapRight = new Image {Source = AppStyle.Icons.TapRigth, HorizontalOptions = LayoutOptions.End };  
			if (isExpand) imgTapRight.RotateTo (90, 400);

			lblTitle = new Label {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				Text = Title,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.White,
				HeightRequest = 40,
				XAlign = TextAlignment.Start,
				YAlign = TextAlignment.Center,
			};

			_skSection.Children.Add (lblTitle);
			_skSection.Children.Add (imgTapRight);
			var _list = new ListView ();
			_list.HasUnevenRows = true;
			_list.RowHeight = 70;
			if (_data != null) {
				_list.ItemsSource = this.ItemSource;
				_list.ItemTemplate = new DataTemplate (typeof(SectionDetailLineal));
				_list.IsVisible = isExpand;
				int count = 0;
				foreach (var item in this.ItemSource) ++count; //TODO: Mejorar esta sentencia
				_list.RowHeight = 50;
				_list.HeightRequest = count * _list.RowHeight;
				_list.HorizontalOptions = LayoutOptions.FillAndExpand;
				_list.VerticalOptions = LayoutOptions.FillAndExpand;
				_list.ItemSelected += (sender, e) => {
					if (_list.SelectedItem == null) return;
					if (SelectedSectionChanged != null)	SelectedSectionChanged(this, ((SectionDTO)_list.SelectedItem));
				};
			}

			if (Device.OS == TargetPlatform.Android) {
				var ftGesture1 = new TapGestureRecognizer ();
				ftGesture1.Tapped += async (object sender, EventArgs e) => {
					_list.IsVisible = !_list.IsVisible;
					if (_list.IsVisible) {
						await imgTapRight.RotateTo(90, 400);
					} else {
						await imgTapRight.RotateTo(0, 400);
					}
					_skSection.BackgroundColor =!_list.IsVisible ? _backgroundContractColor: _backgroundExpandColor;
				};
				_skSection.GestureRecognizers.Add (ftGesture1);
			}

			var skListCourse = new StackLayout {
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.Start
			};
			skListCourse.Children.Add (_list);

			var skOuther = new StackLayout () {
				Padding = new Thickness(0,0,0,0),
				Orientation = StackOrientation.Vertical,
				Children = {_skSection, skListCourse}
			};
			this.Content = skOuther;
		}
	}
}

