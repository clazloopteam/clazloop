﻿using System;
using Xamarin.Forms;

namespace Clazloop.Controls.CourseDetail
{
	public class AuthorCourse: ContentView
	{
		private StackLayout _skAuthor;
		public Label lblAuthor;

		public AuthorCourse (string author)
		{
			lblAuthor = new Label () { 
				Style = AppStyle.AuthorTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				VerticalOptions = LayoutOptions.Center,
				Text = author
			}; 

			_skAuthor = new StackLayout () {
				Padding = new Thickness(5,3),
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = {lblAuthor}
			};
			this.Content = _skAuthor;
		}
	}
}

