﻿using System;
using Xamarin.Forms;

namespace Clazloop.Controls.CourseDetail
{
	public class ProgressBar : ContentView
	{
		private StackLayout _skProgressBar;

		public ProgressBar (Color backgroudcolor, int? pIntProgess)
		{
			_skProgressBar = new StackLayout () {
				BackgroundColor = backgroudcolor,
				Padding = new Thickness(5,5),
				HeightRequest = 12,
				Spacing = 5,
				Orientation = StackOrientation.Horizontal,
			};

			int? pResult = pIntProgess == null ? 0 : pIntProgess / 10;

			if (!(pResult > 0 && pResult < 100)) {
				pResult = 1;
			}

			for (int i = 0; i < pResult; i++) {
				var box = new BoxView () {
					WidthRequest = 10,
					HeightRequest = 10
				};
				switch (i) {
				case 0:
					box.BackgroundColor = Color.FromHex ("98BF2C");
					break;
				case 1:
					box.BackgroundColor = Color.FromHex ("009646");
					break;
				case 2:
					box.BackgroundColor = Color.FromHex ("31B0AC");
					break;
				case 3:
					box.BackgroundColor = Color.FromHex ("006CAC");
					break;
				case 4:
					box.BackgroundColor = Color.FromHex ("614887");
					break;
				case 5:
					box.BackgroundColor = Color.FromHex ("5E278C");
					break;
				case 6:
					box.BackgroundColor = Color.FromHex ("DC0456");
					break;
				case 7:
					box.BackgroundColor = Color.FromHex ("C9232A");
					break;
				case 8:
					box.BackgroundColor = Color.FromHex ("E45729");
					break;
				case 9:
					box.BackgroundColor = Color.FromHex ("F4781B");
					break;
				case 10:
					box.BackgroundColor = Color.FromHex ("D9AD05");
					break;
				default:
					break;
				}
				_skProgressBar.Children.Add (box);
				this.Content = _skProgressBar;
			}
		}
	}
}

