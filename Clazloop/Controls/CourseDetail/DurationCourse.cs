﻿using System;
using Xamarin.Forms;


namespace Clazloop.Controls.CourseDetail
{
	public class DurationCourse : ContentView
	{
		private StackLayout _skDuration;
		public Label lblDuration;
		
		public DurationCourse (string duration)
		{
			lblDuration = new Label () { 
				Style = AppStyle.DurationTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				VerticalOptions = LayoutOptions.Center,
				Text = duration
			}; 

			var imgClock = new Image () {
				Source = AppStyle.Icons.Clock,
				HeightRequest = 12,
				WidthRequest = 12
			};

			_skDuration = new StackLayout () {
				Padding = new Thickness(0,0,0,0),
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = {imgClock, lblDuration}
			};
			this.Content = _skDuration;
		}
	}
}

