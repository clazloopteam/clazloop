﻿using System;
using Xamarin.Forms;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class MenuDTO
	{
		public string Titulo {get; private set;}
		public string Imagen { get; private set; }
		public string Color { get; private set; }

		public MenuDTO(string Titulo, string Imagen)
		{
			this.Titulo = Titulo;
			this.Imagen = Imagen;
			this.Color = "White";
		}
	}

}

