﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class CourseDetailDTO
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }

		[DefaultValue(0)]
		[JsonProperty("totalTime", DefaultValueHandling = DefaultValueHandling.Populate)]
        public string TotalTime { get; set; }

		[DefaultValue(0)]
		[JsonProperty("progress", DefaultValueHandling = DefaultValueHandling.Populate)]
        public int Progress { get; set; }

		[JsonProperty("level")]
		public LevelRestDTO Level { get; set; }

        [DefaultValue(0)]
		[JsonProperty("numberViews", DefaultValueHandling = DefaultValueHandling.Populate)]
		public int NumberViews { get; set; }

		[JsonProperty("authors")]
		public List<AuthorRestDTO> Authors { get; set; }

		[JsonProperty("suggestions")]
		public List<SuggestionDTO> Suggestions { get; set; }

		[JsonProperty("groupSections")]
		public List<GroupSectionDTO> GroupSections { get; set; }


		//[JsonProperty("alreadyPlaylist")]
		//public List<PlayListRestDTO> AlreadyPlaylist { get; set; }

		public string GetDescription
		{
			get 
				{
				return Regex.Replace(System.Net.WebUtility.HtmlDecode (this.Description), @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;", string.Empty).Trim();
				}
		}

		/// <summary>
		/// Buscar el contenido siguiente al ultimo visto.
		/// </summary>
		public SectionDTO FindVideoToView ()
		{
			foreach (var groupSection in this.GroupSections) {
				foreach (var section in groupSection.Sections) {
					if (!section.View) {
						return section;
					}
				}
			}
			return this.GroupSections[0].Sections[0];
		}

		/// <summary>
		/// Buscar el video por el numero.
		/// </summary>
		public SectionDTO FindVideoByVideoNumber (string VideoNumber)
		{
			foreach (var groupSection in this.GroupSections) {
				foreach (var section in groupSection.Sections) {
					if (section.VideoNumber == VideoNumber) {
						return section;
					}
				}
			}
			return null;
		}

		/// <summary>
		/// Obtener el listado de cursos de acuerdo al orden de presentacion.
		/// </summary>
		public List<SectionDTO> GetCourseList
		{
			get {
				List<SectionDTO> auxSectionDTO = new List<SectionDTO>(); 
				if (this.GroupSections == null)
					return null;
				foreach (var groupSection in this.GroupSections) {
					foreach (var section in groupSection.Sections) {
						auxSectionDTO.Add (section);
					}
				}
				return auxSectionDTO;
			}
		}

		public ObservableCollection<SectionDTO> GetSectionList ()
		{
			var aux = new ObservableCollection<SectionDTO>();
			foreach (var groupSection in this.GroupSections) {
				aux.Add (new SectionDTO () { Id = null, 
											 IsSuggest = false,
											 Name = groupSection.GroupName, 
											 IsTitle = true});
				foreach (var section in groupSection.Sections) {
					section.IsSuggest = false;
					aux.Add (section);
				}
			}
			if (this.Suggestions.Count == 0)
				return aux;
			aux.Add (new SectionDTO () { Id = null, Name = "Cursos Recomendados", IsTitle = true, IsSuggest = true});
			foreach (var suggestion in this.Suggestions) {
				aux.Add (new SectionDTO() { 
					Id = suggestion.Id,
					Name = suggestion.Name,
					IsTitle = false,
					Duration = string.Empty,
					View = false,
					IsSuggest = true
				});				
			}
			return aux;
		}
	}
}

