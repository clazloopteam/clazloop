﻿using System;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class CarrerDTO
	{
		public Int64 Id { get; set;}
		public string Name { get; set;}
		public string Description { get; set;}
		public byte[] FlowPhoto { get; set;}

		public CarrerDTO (Int64 Id,string Name,string Description,byte[] FlowPhoto)
		{
			this.Id = Id;
			this.Name = Name;
			this.Description = Description;
			this.FlowPhoto = FlowPhoto;
		}
	}
}

