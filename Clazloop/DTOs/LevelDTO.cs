﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class LevelDTO
	{
		public int Id { get; set;}
		public string Description { get; set;}
		public EnumLevel Level {get; set;}

		public LevelDTO (int Id, string Description, int Level)
		{
			this.Id = Id;
			this.Description = Description;
			this.Level = (EnumLevel)Level;
		}
	}

	public class LevelRestDTO
	{
		[JsonProperty("id")]
		public Int16 Id { get; set;}

		[JsonProperty("name")]
		public string Name { get; set;}

		public LevelRestDTO (Int16 Id, string Descripcion)
		{
			this.Id = Id;
			this.Name = Descripcion;
		}
	}
}

