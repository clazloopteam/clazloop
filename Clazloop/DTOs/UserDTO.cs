﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class UserRestDTO 
	{
		public string Id { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string UrlImage { get; set; }
		public string Token { get; set; }
		public ObservableCollection<CourseRestDTO> coursesHistory { get; set;}
		public ObservableCollection<CourseRestDTO> coursesNew { get; set;}
		public ObservableCollection<CourseRestDTO> coursesSuggestion { get; set;}
		public ObservableCollection<PlayListRestDTO> PlayList { get; set;}
	}
}

