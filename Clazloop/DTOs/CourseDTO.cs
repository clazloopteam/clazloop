﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class CourseRestDTO
	{
		[JsonProperty("id")]
		public Int16 Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("urlImage")]
		public string UrlImage { get; set; }

		[JsonProperty("totalTime")]
		public string TotalTime { get; set; }

		[JsonProperty("leftTime")]
		public string LeftTime { get; set; }

		[JsonProperty("authors")]
		public ObservableCollection <AuthorRestDTO> Authors { get; set; }

		[JsonProperty("playlist")]
		public ObservableCollection <PlayListRestDTO> PlayList { get; set; }

		public string FullUrlImage {
			get {return string.Format ("{0}/{1}",App.CLAZLOOP_DOMAIN,this.UrlImage); }
		}

		public string Named {
			get {return this.Name.Trim (); }
		}

		public string AuthorFullName {
			get {return this.Authors [0].Name; }
		}

		public CourseRestDTO (Int16 Id, string Name, string UrlImage, string TotalTime, string LeftTime)
		{
			this.Id = Id;
			this.Name = Name;
			this.UrlImage = UrlImage;
			this.TotalTime = TotalTime;
			this.LeftTime = LeftTime;
		}
	}

	[Preserve (AllMembers = true)]
	public class CourseRestDTOV2
	{
		[JsonProperty("id")]
		public Int16 Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("urlImage")]
		public string UrlImage { get; set; }

		[JsonProperty("totalTime")]
		public string TotalTime { get; set; }

		[JsonProperty("leftTime")]
		public string LeftTime { get; set; }

		[JsonProperty("level")]
		public LevelRestDTO Level { get; set; }

		[JsonProperty("authors")]
		public ObservableCollection <AuthorRestDTO> Authors { get; set; }

		[JsonProperty("playlist")]
		public ObservableCollection <PlayListRestDTO> PlayList { get; set; }

		[JsonProperty("numberViews")]
		public Int16? NumberViews { get; set; }

		public string FullUrlImage {
			get {return string.Format ("{0}/{1}",App.CLAZLOOP_DOMAIN,this.UrlImage); }
		}

		public string Named {
			get {return this.Name.Trim (); }
		}

		public string AuthorFullName {
			get {return this.Authors [0].Name; }
		}

		public string LevelName {
			get {return this.Level.Name; }
		}

		public CourseRestDTOV2 (Int16 Id, string Name, string UrlImage, string TotalTime, string LeftTime, 
			ObservableCollection <AuthorRestDTO> Authors)
		{
			this.Id = Id;
			this.Name = Name;
			this.UrlImage = UrlImage;
			this.TotalTime = TotalTime;
			this.LeftTime = LeftTime;
			this.Level = Level;
			this.Authors = Authors;
			this.NumberViews = NumberViews;
		}
	}
}

