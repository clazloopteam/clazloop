﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class GroupSectionDTO
	{
		[JsonProperty("groupName")]
		public string GroupName { get; set; }

		[JsonProperty("sections")]
		public List<SectionDTO> Sections { get; set; }
	}
}

