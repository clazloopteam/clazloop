﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class AuthorRestDTO
	{
		public Int16 Id { get; set; }
		public string Name { get; set; }

		public string Letter {
			get { return Name.Substring (0, 1).ToUpper (); }
		}

		public AuthorRestDTO (Int16 Id, string Name)
		{
			this.Id = Id;
			this.Name = Name;
		}
	}
}

