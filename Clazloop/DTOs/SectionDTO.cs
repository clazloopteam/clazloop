﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;


namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class SectionDTO
	{
		private bool _isTitle;
		private bool _isSuggest;


		[JsonProperty("id")]
		public int? Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("duration")]
		public string Duration { get; set; }

		[JsonProperty("urlVideo")]
		public string UrlVideo { get; set; }

		[JsonProperty("view")]
		public bool View { get; set; }

		[JsonProperty("sectionPublic")]
		public int SectionPublic { get; set; }

		[JsonProperty("keyNote")]
		public string KeyNote { get; set; }

		public string VideoNumber
		{
			get { return this.UrlVideo  == string.Empty ? string.Empty :  Regex.Replace(this.UrlVideo, @"[^\d]", ""); }
		}

		public string VideoName
		{
			get { return string.Format("vzvd-{0}", VideoNumber); }
		}

		public string FullVideoUrl 
		{
			get { return VideoNumber == string.Empty ? string.Empty : string.Format ("http://view.vzaar.com/{0}/video", VideoNumber); }
		}

		public string GetVideoHTML
		{
			get {
				return VideoNumber  == string.Empty ? string.Empty : string.Format(@"<html><body><iframe allowFullScreen allowTransparency=""true"" class=""vzaar-video-player"" frameborder=""0"" height=""100%"" id=""{0}"" mozallowfullscreen name=""{1}"" src=""{2}"" title=""{3}"" type=""video/mp4"" webkitAllowFullScreen width=""100%""></iframe></body></html>",this.VideoName, this.VideoName, this.FullVideoUrl, this.Name);
			}
		}

		public ImageSource ImageView
		{
			get { return this.View ? AppStyle.Icons.View : AppStyle.Icons.No_View; }
		}

		public string VideoThumb 
		{
			get { return  VideoNumber  == string.Empty ? string.Empty : string.Format ("http://view.vzaar.com/{0}/thumb", VideoNumber); }
		}

		public string PosterFrame 
		{
			get { return VideoNumber  == string.Empty ? string.Empty :  string.Format ("http://view.vzaar.com/{0}/image", VideoNumber); }
		}

		public bool IsTitle 
		{
			set { _isTitle = value; }
			get { return _isTitle; }
		}

		public bool IsSuggest 
		{
			set { _isSuggest = value; }
			get { return _isSuggest; }
		}

		public Color BackgroundColor {
			get { 
				if (this.IsSuggest && this.IsTitle) {
					return AppStyle.SuggestBackgroundColor;
				} else {
					return  this.IsTitle ? AppStyle.DarkBlue : Color.White; 
				}
			}
		}

		public Color TitleTextColor {
			get {return  this.IsTitle ? Color.White : AppStyle.DarkGrey;  }
		}
	}
}

