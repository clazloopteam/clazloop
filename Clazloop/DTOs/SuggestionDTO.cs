﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class SuggestionDTO
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("urlImage")]
		public string UrlImage { get; set; }

		[JsonProperty("totalTime")]
		public string TotalTime { get; set; }

		//[DefaultValue(0)]
		//[JsonProperty("leftTime", DefaultValueHandling = DefaultValueHandling.Populate)]
        //public int LeftTime { get; set; }

		[JsonProperty("level")]
		public LevelDTO Level { get; set; }

		[JsonProperty("authors")]
		public AuthorRestDTO[] Authors { get; set; }

		[JsonProperty("numberViews")]
		public int NumberViews { get; set; }

		[JsonProperty("alreadyPlaylist")]
		public PlayListRestDTO AlreadyPlaylist { get; set; }
	}
}

