﻿using System;
using Newtonsoft.Json;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class PlayListRestDTO
	{
		private string _name;
		public Int16 Id { get; set; }
		public string Name 
		{ 
			get { return Shared.StringExtensions.ToProper(_name); }
			set { _name = value; }
		}

		[JsonProperty("numCourses")]
		public Int16 NumberCourses { get; set; }

		public string GetCourseNumber {
			get {return string.Format ("{0} Cursos", NumberCourses); }
		}

		public PlayListRestDTO (Int16 Id, string Name, Int16 NumberCouses)
		{
			this.Id = Id;
			this.Name = Name;
			this.NumberCourses = NumberCourses;	
		}
	}
}

