﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class FilterCourseDTO
	{
		[JsonProperty("categoriasHijo")]
		public ObservableCollection<ThematicDTO> CategoriasHijo { get; set;}

		[JsonProperty("cursos")]
		public ObservableCollection<CourseRestDTOV2> Cursos { get; set;}

		[JsonProperty("niveles")]
		public ObservableCollection<LevelDTO> Niveles { get; set;}

		[JsonProperty("instructores")]
		public ObservableCollection<AuthorRestDTO> Instructores { get; set;}

		public FilterCourseDTO ()
		{
			
		}
	}
}

