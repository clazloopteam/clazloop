﻿using System;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class CourseQuestionDTO
	{
		public Int64 Id { get; set;}
		public string Question { get; set;}
		public string Answer { get; set;}
		public bool Public { get; set;}
		public DateTime DateSend { get; set;}
		public DateTime DatePublished { get; set;}
		public UserRestDTO User  { get; set;}

		public CourseQuestionDTO (Int64 Id, string Question, string Answer,  bool Public, DateTime DateSend, DateTime DatePublished)
		{
			this.Id = Id;
			this.Question = Question;
			this.Answer = Answer;
			this.Public = Public;
			this.DateSend = DateSend;
			this.DatePublished = DatePublished;
		}
	}
}

