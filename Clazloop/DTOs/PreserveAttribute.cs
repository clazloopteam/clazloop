﻿using System;

namespace Clazloop.DTOs
{
	public sealed class PreserveAttribute : System.Attribute {
		public bool AllMembers;
		public bool Conditional;
	}
}

