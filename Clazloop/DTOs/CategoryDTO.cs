﻿using System;
using Xamarin.Forms;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class CategoryDTO
	{
		public Int64 Id {get; set;}
		public string Name {get; set;}
		public string Description {get; set;}
		public string Image { get; set; }
		public string Color { get; set; }
		public Int64 IdParent { get; set;}

		public CategoryDTO(Int16 Id, string Name, string Description, string Imagen)
		{
			this.Id = Id;
			this.Name = Name;
			this.Description = Description;
			this.Image = Imagen;
		}

		public CategoryDTO(Int16 Id, string Name, string Description, string Imagen, Int64 IdParent)
		{
			this.Id = Id;
			this.Name = Name;
			this.Description = Description;
			this.Image = Imagen;
			this.IdParent = IdParent;
		}
	}
}

