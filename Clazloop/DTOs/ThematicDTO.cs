﻿using System;

namespace Clazloop.DTOs
{
	[Preserve (AllMembers = true)]
	public class ThematicDTO
	{
		public Int16 Id { get; set;}
		public string Name { get; set;}
		public string Image { get; set; }

		public ThematicDTO (Int16 Id, string Name, string Imagen)
		{
			this.Id = Id;
			this.Name = Name;
			this.Image = Imagen;
		}
	}
}

