﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using XLabs.Forms;
using XLabs.Enums;
using XLabs.Forms.Controls;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Telerik.XamarinForms.Common;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.Primitives.SideDrawer;
using Telerik.XamarinForms.Primitives;
using ImageCircle.Forms.Plugin.Abstractions;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections.Specialized;
using DeviceOrientation.Forms.Plugin.Abstractions;
using System.Threading.Tasks;


namespace Clazloop {
    /// <summary>
    /// CrossVideoPlayerView is a View which contains a MediaElement to play a video.
    /// </summary>

	public delegate void VideoViewDelegate(); 	

    public class CrossVideoPlayerView : View
    {

		public event VideoViewDelegate onVideoViewClick;

		public void OnClickVideo()
		{
			if (onVideoViewClick != null) {
				onVideoViewClick ();
			}
		}

        /// <summary>
        /// The url source of the video.
        /// </summary>
        public static readonly BindableProperty VideoSourceProperty = BindableProperty.Create<CrossVideoPlayerView, string>(p => p.VideoSource, "");

        /// <summary>
        /// The url source of the video.
        /// </summary>
        public string VideoSource
        {
            get
            {
                return (string)GetValue(VideoSourceProperty);
            }
            set
            {
                SetValue(VideoSourceProperty, value);
            }
        }

		/// <summary>
		/// Video Name
		/// </summary>
		public static readonly BindableProperty VideoNameProperty =	BindableProperty.Create<CrossVideoPlayerView, string>(p => p.VideoName, "");
		public string VideoName { get; set;}

		/// <summary>
		/// Name
		/// </summary>
		public static readonly BindableProperty NameProperty = BindableProperty.Create<CrossVideoPlayerView, string>(p => p.Name, "");
		public string Name { get; set;}

		/// <summary>
		/// Buffering Percentage.
		/// </summary>
		public static readonly BindableProperty BufferPercentageProperty = BindableProperty.Create<CrossVideoPlayerView, int>(p => p.BufferPercentage, 0);
		public int BufferPercentage { get; set;}

		/// <summary>
		/// Determinate if video is playing.
		/// </summary>
		public static readonly BindableProperty IsPlayingProperty =	BindableProperty.Create<CrossVideoPlayerView, bool>(p => p.IsPlaying, false);
		public bool IsPlaying { get; set;}

		/// <summary>
        /// The scale format of the video which is in most cases 16:9 (1.77) or 4:3 (1.33).
        /// </summary>
        public static readonly BindableProperty VideoScaleProperty = BindableProperty.Create<CrossVideoPlayerView, double>(p => p.VideoScale, 1.77);
	
        /// <summary>
        /// The scale format of the video which is in most cases 16:9 (1.77) or 4:3 (1.33).
        /// </summary>
        public double VideoScale
        {
            get
            {
                return (double)GetValue(VideoScaleProperty);
            }
            set
            {
                SetValue(VideoScaleProperty, value);
            }
        }
    }
}
