﻿using Xamarin.Forms;
using System.Collections.ObjectModel;
using Clazloop.DTOs;

namespace Clazloop
{
	public class App : Application
	{
		public static double ScreenWidth;
		public static double ScreenHeight;

		public const string CLAZLOOP_DOMAIN = "https://clazloop.com";
		public static LevelRestDTO FiltroNivel;
		public static ThematicDTO FiltroTematica;
		public static AuthorRestDTO FiltroAutor;
		public static ObservableCollection<ThematicDTO> FiltroSubTematica;
		public static ObservableCollection<AuthorRestDTO> FiltroTematicaAutor;
		public static ObservableCollection<ThematicDTO> ThematicList;
		public static int Page;
		public static UserRestDTO UserInfo;
		public static bool IsLogin;


		public App ()
		{	
			var newNavigationPage =  new NavigationPage(new Login());	
			newNavigationPage.BarTextColor = Color.White;
			MainPage = newNavigationPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

