﻿using System;
using Clazloop;
using Xamarin.Forms;

namespace Clazloop
{
	public enum VideoState
	{
		STOP,
		RESTART,
		PAUSE,
		PLAY,
		ENDED,
		NONE
	}

	public class VideoViewOS : View
	{
		public Action TogglePlay { get; set; }
		public Action Stop { get; set; }
		public Action Play { get; set; }
		public Action HideFullScreenButton { get; set; }

		public event EventHandler<bool> PlaybackStateChanged;
		public event EventHandler PlaybackFinished;

		//ContentView videoPlayer;
		/// <summary>
		/// The url source of the video.
		/// </summary>
		public static readonly BindableProperty VideoSourceProperty = BindableProperty.Create<VideoViewOS, string>(p => p.VideoSource, "");

		/// <summary>
		/// The url source of the video.
		/// </summary>
		public string VideoSource
		{
			get	{ return (string)GetValue(VideoSourceProperty);	}
			set	{ SetValue(VideoSourceProperty, value);	}
		}


		public static readonly BindableProperty PlayerActionProperty = BindableProperty.Create<VideoViewOS,VideoState>(p => p.PlayerAction, VideoState.NONE);	
		public VideoState PlayerAction {
			get { return (VideoState)GetValue (PlayerActionProperty); }
			set { 
				// fire change always
				if (value != PlayerAction) {
					SetValue (PlayerActionProperty, value); 
				} else {
					OnPropertyChanged (PlayerActionProperty.PropertyName);
				}
			}
		}

		public static BindableProperty FullscreenProperty = BindableProperty.Create<VideoViewOS, bool>(o => o.Fullscreen, false);
		public bool Fullscreen
		{
			get { return (bool)GetValue(FullscreenProperty); }
			set { SetValue(FullscreenProperty, value); }
		}


		public static BindableProperty ShowControlsProperty = BindableProperty.Create<VideoViewOS, bool>(o => o.ShowControls, true);
		public bool ShowControls
		{
			get { return (bool) GetValue(ShowControlsProperty); }
			set { SetValue(ShowControlsProperty, value); }
		}

	}
}