﻿using Xamarin.Forms;
using Telerik.XamarinForms.DataControls.ListView;
using Clazloop.DTOs;

namespace Clazloop.ListViewTemplates
{
	[Preserve (AllMembers = true)]
	public class LevelViewCell : ListViewTemplateCell
	{
		public LevelViewCell()
		{
			var lblTitulo = new Label () {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				TextColor = AppStyle.DarkBlue,
				HorizontalOptions = LayoutOptions.StartAndExpand, 
				VerticalOptions = LayoutOptions.CenterAndExpand 
			};
			lblTitulo.SetBinding (Label.TextProperty, "Name");

			var tapImage = new Image () {
				Source = AppStyle.Icons.Tap,
				HorizontalOptions = LayoutOptions.End
			};

			var outerStack = new StackLayout { 
				Orientation = StackOrientation.Horizontal,
				Children = {lblTitulo, tapImage }
			};
			this.View = outerStack;
		}	
	}
}

