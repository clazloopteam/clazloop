﻿using System;
using Xamarin.Forms;
using Clazloop.DTOs;

namespace Clazloop.ListViewTemplates
{

	[Preserve(AllMembers = true)]
	public class SectionDetailLineal : ViewCell
	{
		public SectionDetailLineal()
		{
			var grid = new Grid
			{
				Padding = new Thickness(5),
				ColumnSpacing = 0,
				RowSpacing = 0,
				RowDefinitions = { new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) } },
				ColumnDefinitions = {
					new ColumnDefinition { Width = new GridLength(5, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(80, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(15, GridUnitType.Star) }
				}
			};
			var lblTitle = new Label()
			{
				Style = AppStyle.CourseTitleLinearStyle,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				TextColor = AppStyle.DarkGrey,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				LineBreakMode = LineBreakMode.TailTruncation

			};
			lblTitle.SetBinding(Label.TextProperty, "Name");
			var skTitle = new StackLayout()
			{
				Padding = new Thickness(5)
			};
			skTitle.Children.Add(lblTitle);

			var lblDuration = new Label()
			{
				Style = AppStyle.CourseTitleLinearStyle,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				TextColor = AppStyle.DarkGrey,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.Center
			};
			lblDuration.SetBinding(Label.TextProperty, "Duration");

			Image imgEye = new Image();
			imgEye.SetBinding(Image.SourceProperty, "ImageView");

			grid.Children.Add(imgEye, 0, 0);
			grid.Children.Add(skTitle, 1, 0);
			grid.Children.Add(lblDuration, 2, 0);
			this.View = grid;
		}
	}
}

