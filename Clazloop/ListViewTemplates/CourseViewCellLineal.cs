﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.DataControls.ListView;
using Clazloop.DTOs;

namespace Clazloop.ListViewTemplates
{
	[Preserve(AllMembers = true)]
	public class CourseViewCellLineal : ListViewTemplateCell
	{
		public CourseViewCellLineal()
		{
			Image imgCourse = new Image();
			imgCourse.SetBinding(Image.SourceProperty, "FullUrlImage");
			imgCourse.Aspect = Aspect.AspectFill;

			var skImage = new StackLayout
			{
				Padding = 2,
				WidthRequest = AppStyle.ImageWidthRelation,
				HeightRequest = AppStyle.ImageHeightRelation,
				Children = { imgCourse }
			};

			var lblNombre = new Label()
			{
				Style = AppStyle.CourseTitleLinearStyle,
				LineBreakMode = LineBreakMode.TailTruncation
			};
			lblNombre.SetBinding(Label.TextProperty, "Named");
			var skNombre = new StackLayout
			{
				Orientation = StackOrientation.Horizontal
			};
			skNombre.Children.Add(lblNombre);

			var lblAutor = new Label()
			{
				Style = AppStyle.AuthorTitleStyle,
				LineBreakMode = LineBreakMode.TailTruncation
			};
			lblAutor.SetBinding(Label.TextProperty, "AuthorFullName");

			var lblDuracion = new Label()
			{
				Style = AppStyle.DurationTitleStyle,
				LineBreakMode = LineBreakMode.TailTruncation
			};
			lblDuracion.SetBinding(Label.TextProperty, "TotalTime");

			var imgClock = new Image()
			{
				Source = AppStyle.Icons.Clock,
				HeightRequest = 10,
				WidthRequest = 10
			};

			var lblNivel = new Label()
			{
				Style = AppStyle.DurationTitleStyle,
				LineBreakMode = LineBreakMode.TailTruncation
			};
			lblNivel.Text = "Sin definir";
			//lblNivel.SetBinding (Label.TextProperty, "LevelName");

			var imgNivel = new Image()
			{
				Source = AppStyle.Icons.Level,
				HeightRequest = 10,
				WidthRequest = 10
			};

			var stkDuracion = new StackLayout()
			{
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = { imgClock, lblDuracion, imgNivel, lblNivel }
			};

			var skDetalles = new StackLayout
			{
				Padding = 0,
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { skNombre, lblAutor, stkDuracion }
			};

			var skInnerCell = new StackLayout
			{
				Padding = 0,
				Orientation = StackOrientation.Horizontal,
				Children = { skImage, skDetalles }
			};
			this.View = skInnerCell;
		}
	}
}

