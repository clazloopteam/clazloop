﻿using Xamarin.Forms;
using Telerik.XamarinForms.DataControls.ListView;
using Clazloop.DTOs;

namespace Clazloop.ListViewTemplates
{
	[Preserve (AllMembers = true)]
	public class AuthorViewCellLineal : ListViewTemplateCell
	{
		public AuthorViewCellLineal()
		{
			var lblAutor = new Label () {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = AppStyle.DarkBlue,
			};

			lblAutor.SetBinding (Label.TextProperty, "Name");

			var lblCantidadCursos = new Label () {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = AppStyle.DarkGrey,
				Text = "Cantidad Total de Cursos"
			};

			var lblDescripcion = new Label () {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = AppStyle.DarkGrey,
				Text = "Descripcion"
			};
			//lblDescripcion.SetBinding (Label.TextProperty, "Descripction");

			var skDetalle = new StackLayout{ 
				Spacing = 3,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {lblAutor,lblCantidadCursos, lblDescripcion}
			};

			var skInstructor = new StackLayout () {
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = {  skDetalle }
			};

			var innerStack = new StackLayout {
				Spacing = 5,
				Padding = new Thickness (10, 5, 10, 5),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { /*vetProfileImage,*/ skInstructor}
			};

			var outterStack = new StackLayout { 
				Orientation = StackOrientation.Vertical,
				Children = {innerStack}
			};

			this.View = outterStack;
		}
	}		
}

