﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.DataControls.ListView;
using Clazloop.DTOs;

namespace Clazloop.ListViewTemplates
{
	[Preserve(AllMembers = true)]
	public class PlayListCardView : ListViewTemplateCell
	{
		public PlayListCardView()
		{
			Image imgCourse = new Image();
			imgCourse.Aspect = Aspect.AspectFill;
			imgCourse.Source = new UriImageSource
			{
				CachingEnabled = true,
				CacheValidity = new TimeSpan(5, 0, 0, 0),
			};
			imgCourse.Source = AppStyle.Icons.PlayList;

			var skImage = new StackLayout
			{
				Padding = 2,
				BackgroundColor = Color.Transparent,
				WidthRequest = AppStyle.ImageWidthRelation,
				HeightRequest = AppStyle.ImageHeightRelation,
				Children = { imgCourse }
			};

			var lblNombre = new Label()
			{
				Style = AppStyle.CourseTitleStyle,
				LineBreakMode = LineBreakMode.CharacterWrap
			};
			lblNombre.SetBinding(Label.TextProperty, "Name");

			var lblCantidadCursos = new Label()
			{
				Style = AppStyle.AuthorTitleStyle,
				FontAttributes = FontAttributes.Bold
			};
			lblCantidadCursos.SetBinding(Label.TextProperty, "GetCourseNumber");

			var skDescripcion = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				Spacing = 0,
				Padding = 10,
				Children = { lblNombre, lblCantidadCursos }
			};

			var skDetalles = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				Children = { skDescripcion }
			};

			var cellLayout = new StackLayout
			{
				Padding = 2d,
				BackgroundColor = Color.Transparent,
				WidthRequest = App.ScreenWidth / 3,
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				Children = { skImage, skDetalles }
			};
			this.View = cellLayout;
		}
	}
}

