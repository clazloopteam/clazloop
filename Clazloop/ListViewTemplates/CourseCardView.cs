﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.DataControls.ListView;
using Clazloop.DTOs;

namespace Clazloop.ListViewTemplates
{
	[Preserve(AllMembers = true)]
	public class CourseCardView : ListViewTemplateCell
	{
		public CourseCardView()
		{
			Image imgCourse = new Image();
			imgCourse.WidthRequest = AppStyle.ImageWidthRelation;
			imgCourse.HeightRequest = AppStyle.ImageHeightRelation;
			imgCourse.Aspect = Aspect.AspectFill;
			imgCourse.Source = new UriImageSource
			{
				CachingEnabled = true,
				CacheValidity = new TimeSpan(5, 0, 0, 0),
			};
			imgCourse.SetBinding(Image.SourceProperty, "FullUrlImage");

			var skImage = new StackLayout
			{
				Padding = 0,
				BackgroundColor = Color.Transparent,
				WidthRequest = AppStyle.ImageWidthRelation,
				HeightRequest = AppStyle.ImageHeightRelation,
				Children = { imgCourse }
			};

			var lblNombre = new Label()
			{
				Style = AppStyle.CourseTitleStyle,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				LineBreakMode = LineBreakMode.TailTruncation
			};
			lblNombre.SetBinding(Label.TextProperty, "Named");

			var skNombre = new StackLayout
			{
				Orientation = StackOrientation.Horizontal
			};
			skNombre.Children.Add(lblNombre);

			var lblAutor = new Label()
			{
				Style = AppStyle.AuthorTitleStyle,
				LineBreakMode = LineBreakMode.TailTruncation
			};
			lblAutor.SetBinding(Label.TextProperty, "AuthorFullName");

			var lblDuracion = new Label()
			{
				Style = AppStyle.DurationTitleStyle
			};
			lblDuracion.SetBinding(Label.TextProperty, "TotalTime");

			var imgClock = new Image()
			{
				Source = "ic_clock.png",
				HeightRequest = 12,
				WidthRequest = 12
			};

			var lblNivel = new Label()
			{
				Style = AppStyle.DurationTitleStyle,
				Text = "Basico"
			};
			//lblNivel.SetBinding (Label.TextProperty, "Level");

			var imgNivel = new Image()
			{
				Source = "ic_level.png",
				HeightRequest = 12,
				WidthRequest = 12
			};

			var stkDuracion = new StackLayout()
			{
				Padding = new Thickness(0, 5, 0, 10),
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = { imgClock, lblDuracion, imgNivel, lblNivel }
			};

			var skDescripcion = new StackLayout
			{
				Padding = new Thickness(5, 0, 5, 0),
				Orientation = StackOrientation.Vertical,
				Spacing = 0,
				Children = { skNombre, lblAutor, stkDuracion }
			};

			var skDetalles = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				Children = { skDescripcion }
			};

			var cellLayout = new StackLayout
			{
				Padding = 2d,
				BackgroundColor = Color.Transparent,
				WidthRequest = App.ScreenWidth / 3,
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				Children = { skImage, skDetalles }
			};
			this.View = cellLayout;
		}
	}
}

