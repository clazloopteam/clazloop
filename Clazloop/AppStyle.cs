﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.Common;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.Primitives.SideDrawer;
using Telerik.XamarinForms.Primitives;
using Telerik.XamarinForms.DataControls.ListView;

namespace Clazloop
{
	public static class AppStyle
	{
		public static Color LightBlue = Color.FromHex ("B4D6D4");
		public static Color DarkBlue = Color.FromHex ("6AAEAA");
		public static Color LightGrey = Color.FromHex("E6E6E6");
		public static Color DarkGrey = Color.FromHex("7A7A7A");
		public static Color Grey = Color.FromHex("D2D2D2");
		public static Color BlueBackgroundColor = Color.FromHex ("156FA7");
		public static Color SuggestBackgroundColor = Color.FromHex ("006CA8");
		public static Color FilterBackgroundColor = Color.FromHex ("457471");
		public static Color Orange = Color.FromHex("E3AD25");
		public static Color Fucsia =  Color.FromHex("A91460");
		public static Color FacebookBlue = Color.FromHex("3B5999");
		public static Color GoogleRed = Color.FromHex("DC4A38");
		public static Color LigthSection = Color.FromHex("B5D7D5"); //NOTE: Seccion Cerrada
		public static Color DarkSection = Color.FromHex("6BAFAB"); //NOTE: Seccion Abierta

		public static double ImageWidthRelation
		{
			get {
				if (Device.OS == TargetPlatform.Android)
				{
					return App.ScreenWidth / 3;
				} else { 
					return App.ScreenWidth / 3;
				}
			}
		}

		public static double ImageHeightRelation
		{
			get {
				if (Device.OS == TargetPlatform.Android)
				{
					return App.ScreenWidth / 5;
				} else { 
					return App.ScreenWidth / 3;
				}
			}
		}


		public static string FontFamily
		{
			get { 
				if (Device.OS == TargetPlatform.Android)
				{
					return "sans-serif-condensed";
				} else { 
					return "HelveticaNeue-CondensedBold";
				}		
			}
		}

		public static Style CourseTitleStyle {
			get { 
				Style style = new Style (typeof(Label));
				style.Setters.Add (new Setter{ Property = Label.FontSizeProperty, Value = Device.OS == TargetPlatform.Android ? 16 : 14 });
				style.Setters.Add (new Setter{ Property = Label.FontFamilyProperty, Value = Device.OnPlatform ("HelveticaNeue-CondensedBold", "sans-serif-condensed", null) });
				style.Setters.Add (new Setter{ Property = Label.TextColorProperty, Value =  Color.FromHex("136EA6") });
				//style.Setters.Add (new Setter{ Property = Label.HeightRequestProperty, Value = 43 });
				style.Setters.Add (new Setter{ Property = Label.XAlignProperty, Value = TextAlignment.Start });
				return style;
			}
		}

		public static Style CourseTitleLinearStyle {
			get { 
				Style style = new Style (typeof(Label));
				style.Setters.Add (new Setter{ Property = Label.FontSizeProperty, Value = 13 });
				style.Setters.Add (new Setter{ Property = Label.FontFamilyProperty, Value = Device.OnPlatform ("HelveticaNeue-CondensedBold", "sans-serif-condensed", null) });
				style.Setters.Add (new Setter{ Property = Label.TextColorProperty, Value =  Color.FromHex("136EA6") });
				style.Setters.Add (new Setter{ Property = Label.XAlignProperty, Value = TextAlignment.Start });
				return style;
			}
		}

		public static Style AuthorTitleStyle {
			get { 
				Style style = new Style (typeof(Label));
				style.Setters.Add (new Setter{ Property = Label.FontSizeProperty, Value = 11 });
				style.Setters.Add (new Setter{ Property = Label.FontFamilyProperty, Value = Device.OnPlatform ("HelveticaNeue-CondensedBold", "sans-serif-condensed", null) });
				style.Setters.Add (new Setter{ Property = Label.TextColorProperty, Value =  Color.FromHex ("7B7B7B")  }); 
				style.Setters.Add (new Setter{ Property = Label.XAlignProperty, Value = TextAlignment.Start });
				return style;
			}
		}

		public static Style DurationTitleStyle {
			get { 
				Style style = new Style (typeof(Label));
				style.Setters.Add (new Setter{ Property = Label.FontSizeProperty, Value = 9 });
				style.Setters.Add (new Setter{ Property = Label.FontFamilyProperty, Value = Device.OnPlatform ("HelveticaNeue-CondensedBold", "sans-serif-condensed", null) });
				style.Setters.Add (new Setter{ Property = Label.TextColorProperty, Value =   Color.FromHex ("7B7B7B")  }); 
				style.Setters.Add (new Setter{ Property = Label.XAlignProperty, Value = TextAlignment.Start });
				return style;
			}
		}

		public static Style DashBoardTitleStyle {
			get { 
				Style style = new Style (typeof(Label));
				style.Setters.Add (new Setter{ Property = Label.FontSizeProperty, Value = 16 });
				style.Setters.Add (new Setter{ Property = Label.FontFamilyProperty, Value = Device.OnPlatform ("HelveticaNeue-CondensedBold", "sans-serif-condensed", null) });
				style.Setters.Add (new Setter{ Property = Label.TextColorProperty, Value =  LightBlue  }); 
				style.Setters.Add (new Setter{ Property = Label.FontAttributesProperty, Value = FontAttributes.Bold }); 
				style.Setters.Add (new Setter{ Property = Label.XAlignProperty, Value = TextAlignment.Start });
				return style;
			}
		}

		public static Style DashBoardSubTitleStyle {
			get { 
				Style style = new Style (typeof(Label));
				style.Setters.Add (new Setter{ Property = Label.FontSizeProperty, Value = 16 });
				style.Setters.Add (new Setter{ Property = Label.FontFamilyProperty, Value = Device.OnPlatform ("HelveticaNeue-CondensedBold", "sans-serif-condensed", null) });
				style.Setters.Add (new Setter{ Property = Label.TextColorProperty, Value =  DarkBlue  }); 
				style.Setters.Add (new Setter{ Property = Label.FontAttributesProperty, Value = FontAttributes.Bold }); 
				style.Setters.Add (new Setter{ Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.EndAndExpand});
				return style;
			}
		}

		public static ListViewItemStyle ListViewLinearLayoutAllBorder {
			get { 
				ListViewItemStyle lstStyle = new ListViewItemStyle (){ 
					BorderWidth = 2d,
					BorderLocation = Location.All,
					BorderColor = AppStyle.LightGrey,
					BackgroundColor = Color.Transparent
				};
				return lstStyle;
			}
		}

		public static ListViewItemStyle ListViewLinearLayouBottomBorder {
			get { 
				ListViewItemStyle lstStyle = new ListViewItemStyle (){ 
					BorderWidth = 2d,
					BorderLocation = Location.Bottom,
					BorderColor = AppStyle.LightGrey,
					BackgroundColor = Color.White
				};
				return lstStyle;
			}
		}

		public static ListViewItemStyle ListViewTransparentBorder {
			get { 
				ListViewItemStyle lstStyle = new ListViewItemStyle () {
					BorderLocation = Location.All,
					BorderColor = Color.Transparent,
					BackgroundColor = Color.White,
					BorderWidth = 0
				};
				return lstStyle;
			}
		}

		public class Icons
		{
			public static FileImageSource Tap = new FileImageSource () { File = "tap_image.png" };
			public static FileImageSource TapDown = new FileImageSource () { File = "tap_image_down.png" };
			public static FileImageSource TapRigth = new FileImageSource () { File = "tap_image_rigth.png" };
			public static FileImageSource BlueBar = new FileImageSource () { File = "blue_bar.png" };
			public static FileImageSource Profile = new FileImageSource () { File = "ic_profile.png" };
			public static FileImageSource BannerTop = new FileImageSource () { File = "ic_searching_2.png" };
			public static FileImageSource BannerBottom = new FileImageSource () { File = "ic_searching_1.png" };
			public static FileImageSource PlayList = new FileImageSource () { File = "folder_playlist_3.png" };
			public static FileImageSource Clock = new FileImageSource () { File = "ic_clock.png" };
			public static FileImageSource Level = new FileImageSource () { File = "ic_level.png" };
			public static FileImageSource Logo = new FileImageSource () { File = "clazloop_logo.png" };
			public static FileImageSource Facebook = new FileImageSource () { File = "ico_facebook.png" };
			public static FileImageSource GooglePlus = new FileImageSource () { File = "ico_google.png" };
			public static FileImageSource View = new FileImageSource () { File = "ic_eye.png" };
			public static FileImageSource No_View = new FileImageSource () { File = "ic_no_eye.png" };
		}
	}
}

