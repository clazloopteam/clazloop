﻿using System;
using Xamarin.Forms;
using Clazloop.DTOs;

namespace Clazloop
{
	public class RootPage : MasterDetailPage
	{
		public RootPage ()
		{
			NavigationPage.SetHasNavigationBar(this, false);
			var menuPage = new MenuPage ();
			menuPage.Menu.ItemSelected += (sender, e) => {
				NavigateTo (e.SelectedItem as MenuItem);
			};
			Master = menuPage;
			var navPage = new NavigationPage (new DashBoardPage ());
			navPage.BarTextColor = Color.White;
			Detail = navPage; //new NavigationPage (new DashBoardPage ());
		}

		void NavigateTo (MenuItem menu)
		{
			NavigationPage navPage;
			EnumElementoDashBoard Tipo; 
			if (menu.TargetType == null) return;
			if (menu.TargetType.FullName == "Clazloop.CursosList") {
				switch (menu.Title.Trim()) {
					case "Historial":
						Tipo = EnumElementoDashBoard.HISTORIAL;
						break;
					case "Favoritos":
						Tipo = EnumElementoDashBoard.FAVORITOS;
						break;
					default:
						Tipo = EnumElementoDashBoard.HISTORIAL;
						break;
				}
				navPage = new NavigationPage (new CursosList(App.UserInfo.coursesHistory,Tipo));
				navPage.BarTextColor = Color.White;
				Detail = navPage;  
			} else if (menu.TargetType.FullName == "Clazloop.CategoriaPage") {
				App.FiltroTematica = new ThematicDTO (4, "Clazbooks", string.Empty);
				App.Page = 0;
				App.FiltroSubTematica = null;
				App.FiltroAutor = null;
				App.FiltroNivel = null;
				navPage = new NavigationPage (new CategoriaPage ());
				navPage.BarTextColor = Color.White;
				Detail = navPage;
			} else {
				Page displayPage = (Page)Activator.CreateInstance (menu.TargetType);
				navPage = new NavigationPage (displayPage);
				navPage.BarTextColor = Color.White;
				Detail = navPage;
			}
			IsPresented = false;
		}
	}
}