﻿using System;
using System.Collections.Generic;

namespace Clazloop
{
	public class Menu
	{
		public Menu ()
		{
			
		}

		public List<MenuDTO> ObtenerOpcionesMenu()
		{
			var aux = new List<MenuDTO>();
			aux.Add(new MenuDTO("Hola, Edward Vargas",""));
			aux.Add(new MenuDTO("Inicio","blue_bar.png"));
			aux.Add (new MenuDTO ("Biblioteca", "blue_bar.png.png"));
			aux.Add (new MenuDTO ("Mis Cursos","blue_bar.png.png"));
			aux.Add (new MenuDTO ("Ajustes", "blue_bar.png.png"));
			aux.Add (new MenuDTO ("Contactanos", "blue_bar.png.png"));
			aux.Add (new MenuDTO ("Cerrar Sesión", "blue_bar.png.png"));
			return aux;
		}

	}
}

