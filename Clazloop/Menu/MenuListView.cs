﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using Clazloop.DTOs;

namespace Clazloop
{
	public class MenuListView : ListView
	{
		public MenuListView ()
		{
			List<MenuItem> data = new MenuListData ();
			SeparatorColor = Color.Transparent;
			RowHeight = 40;
			ItemsSource = data;
			VerticalOptions = LayoutOptions.FillAndExpand;
			BackgroundColor = Color.Transparent;
			ItemTemplate = new DataTemplate (typeof(MenuCell));
		}
	}

	[Preserve(AllMembers=true)]
	public class MenuCell : ViewCell
	{ 
		public MenuCell()
		{
			Image imgIconoMenu = new Image ();
			imgIconoMenu.SetBinding (Image.SourceProperty, "IconSource");

			var lblTitulo = new Label () {
				TextColor = AppStyle.Grey,
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				HorizontalOptions = LayoutOptions.StartAndExpand, 
				VerticalOptions = LayoutOptions.CenterAndExpand 
			};
			lblTitulo.SetBinding (Label.TextProperty, "Title");

			var vetDetailsLayout = new StackLayout {
				Padding = new Thickness (10, 0, 0, 0),
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { lblTitulo }
			};
					
			var cellLayout = new StackLayout {
				Spacing = 5,
				Padding = new Thickness (20, 5, 10, 5),
				Orientation = StackOrientation.Horizontal,
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { imgIconoMenu, vetDetailsLayout }
			};
			this.View = cellLayout;
		}
	}
}
