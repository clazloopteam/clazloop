﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using ImageCircle.Forms.Plugin.Abstractions;

namespace Clazloop
{
	public class MenuPage : ContentPage
	{
		public MenuListView Menu { get; set; }

		public MenuPage ()
		{
			if (Device.OS == TargetPlatform.iOS) {
				this.Padding = new Thickness(5, Device.OnPlatform(20, 0, 0), 5, 5);
			}
			this.Icon = "hamburger_.png";
			this.Title = "menu";
			if (Device.OS == TargetPlatform.Android) this.BackgroundImage = "drawer_background.png"; 
			this.BackgroundColor = AppStyle.BlueBackgroundColor;
			Menu = new MenuListView ();

			var layout = new StackLayout { 
				Spacing = 0, 
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			Image imgBlueBar = new Image {Source = AppStyle.Icons.BlueBar}; 

			var vetProfileImage = new CircleImage {
				BorderColor = Color.White,
				BorderThickness = 4,
				HeightRequest = 50,
				WidthRequest = 50,
				Aspect = Aspect.AspectFill,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Source = ImageSource.FromUri(new Uri(string.Format("{0}/{1}",App.CLAZLOOP_DOMAIN,App.UserInfo.UrlImage)))
			};

			var layoutProfile = new StackLayout { 
				Padding = 10,
				Spacing = 5,
				Orientation = StackOrientation.Horizontal,
			};

			Label lblMensajeBienvenida = new Label  { 
				Text = string.Format("Bienvenido, {0}",App.UserInfo.Name), 
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = Color.White, 
		        FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center, 
				VerticalOptions = LayoutOptions.CenterAndExpand 
			};
			layoutProfile.Children.Add (vetProfileImage);
			layoutProfile.Children.Add (lblMensajeBienvenida);


			Label lblSeparador = new Label { Text = "", TextColor = Color.White };

			layout.Children.Add (imgBlueBar);
			layout.Children.Add (lblSeparador);
			layout.Children.Add (layoutProfile);
			layout.Children.Add (lblSeparador);
			layout.Children.Add (Menu);
			Content = layout;
		}
	}
}

