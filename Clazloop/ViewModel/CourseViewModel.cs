﻿using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Clazloop.DTOs;
using System.Collections.ObjectModel;

namespace Clazloop
{
	public class CourseViewModel : BaseViewModel
	{
		public CourseViewModel ()
		{
			
		}

		public async Task <CourseDetailDTO> GetCourseInfo(string UserName, string Token, Int16 IdCourse)
		{
			try 
			{
				string Url = string.Format ("{0}/clazloop-service/rest/coursesService/detailCourse?username={1}&token={2}&idCourse={3}",App.CLAZLOOP_DOMAIN,UserName,Token, IdCourse);
				var client = new System.Net.Http.HttpClient ();
				var response = await client.GetAsync(Url);
				var courseRest = response.Content.ReadAsStringAsync().Result;
				var webResult = JsonConvert.DeserializeObject<WebServiceResult>(courseRest);
				if (webResult.data == null)
					return null;
				CourseDetailDTO courseInfo = JsonConvert.DeserializeObject<CourseDetailDTO>(webResult.data);
				return courseInfo;
			} catch (Exception ex) {
				return null;	
			}
		}

		public async Task<ObservableCollection<CourseRestDTO>> SearchCoursesPlayList(string UserName, string Token, Int16 IdPlayList)
		{
			try
			{
				string Url = string.Format("{0}/clazloop-service/rest/coursesService/searchCoursesPlaylist?username={1}&token={2}&idPlayList={3}", App.CLAZLOOP_DOMAIN, UserName, Token, IdPlayList);
				var client = new System.Net.Http.HttpClient();
				var response = await client.GetAsync(Url);
				var courseRest = response.Content.ReadAsStringAsync().Result;
				var webResult = JsonConvert.DeserializeObject<WebServiceResult>(courseRest);
				if (webResult.data == null)
					return null;
				ObservableCollection<CourseRestDTO> courseInfo = JsonConvert.DeserializeObject<ObservableCollection<CourseRestDTO>>(webResult.data);
				return courseInfo;
			}
			catch (Exception ex)
			{
				return null;
			}
		}
	}
}

