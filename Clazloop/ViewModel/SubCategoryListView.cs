﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using Telerik.XamarinForms.Common;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.Primitives.SideDrawer;
using Telerik.XamarinForms.Primitives;
using ImageCircle.Forms.Plugin.Abstractions;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections.Specialized;

namespace Clazloop
{
	public class SubCategoryListView: RadListView
	{
		public SubCategoryListView ()
		{
			this.SelectionGesture = SelectionGesture.Tap;
			this.SelectionMode = SelectionMode.Single;
			this.VerticalOptions = LayoutOptions.FillAndExpand;
			this.HorizontalOptions = LayoutOptions.FillAndExpand;
		}
	}

	public class SubCatergoryViewCell : ListViewTemplateCell
	{
		public SubCatergoryViewCell()
		{
			Image imgLevel = new Image ();
			imgLevel.Source = "ic_level_2.png"; 

			var lblTitulo = new ExtLabel () {
				TextColor = Color.FromHex("136EA6"),
				FontFamily = "ArchivoNarrow-Bold",
				FontSize = 16d,
				HorizontalOptions = LayoutOptions.StartAndExpand, 
				VerticalOptions = LayoutOptions.CenterAndExpand 
			};
			lblTitulo.Text = "Nivel del Curso";
			lblTitulo.SetBinding (Label.TextProperty, "Description");

			var vetDetailsLayout = new StackLayout {
				Padding = new Thickness (10, 0, 0, 0),
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { lblTitulo }
			};

			var tapImage = new Image () {
				Source = "tap_image.png",
				HorizontalOptions = LayoutOptions.End
			};

			var cellLayout = new StackLayout {
				BackgroundColor = Color.White,
				Spacing = 0,
				Padding = new Thickness (10, 5, 10, 5),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { imgLevel, vetDetailsLayout, tapImage }
			};
			this.View = cellLayout;
		}	
	}
}

