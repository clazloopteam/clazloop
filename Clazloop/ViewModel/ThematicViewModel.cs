﻿using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Clazloop.DTOs;

namespace Clazloop
{
	public class ThematicViewModel : BaseViewModel
	{
		public ThematicViewModel()
		{

		}

		public async Task <ObservableCollection<ThematicDTO>> GetThematicsInfo(string UserName, string Token)
		{
			try 
			{
				if (String.IsNullOrEmpty(Token)) return null;
				string Url = string.Format ("{0}/clazloop-service/rest/coursesService/allThematic?username={1}&token={2}",App.CLAZLOOP_DOMAIN,UserName,Token);
				var client = new System.Net.Http.HttpClient ();
				var response = await client.GetAsync(Url);
				var thematicsRest = response.Content.ReadAsStringAsync().Result;
				var webResult = JsonConvert.DeserializeObject<WebServiceResult>(thematicsRest);
				if (webResult.data == null)
					return null;
				ObservableCollection<ThematicDTO> thematicsInfo = JsonConvert.DeserializeObject<ObservableCollection<ThematicDTO>>(webResult.data);
				return thematicsInfo;
			} catch (Exception ex) {
				return null;	
			}
		}	
	}
}

