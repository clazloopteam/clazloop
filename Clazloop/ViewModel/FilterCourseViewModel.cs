﻿using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Clazloop.DTOs;

namespace Clazloop
{
	public class FilterCourseViewModel : BaseViewModel
	{
		public FilterCourseViewModel ()
		{
			
		}

		public async Task <FilterCourseDTO> GetCourses(string UserName, string Token, Int16 IdThematic, Int16 IdAuthor, 
			                                           Int16 IdLevel, string Name, int Pagina)
		{
			try 
			{
				if (String.IsNullOrEmpty(Token)) return null;
				string Url = string.Format ("{0}/clazloop-service/rest/coursesService/coursesByFilter?username={1}&token={2}&idCategory={3}&idAuthor={4}&idLevel={5}&name={6}&pagina={7}",
					App.CLAZLOOP_DOMAIN,UserName,Token,(IdThematic == 0 ? "": IdThematic.ToString())  ,(IdAuthor == 0?"":IdAuthor.ToString()),(IdLevel == 0? "": IdLevel.ToString()),Name,
					Pagina.ToString());
				var client = new System.Net.Http.HttpClient ();
				var response = await client.GetAsync(Url);
				var userRest = response.Content.ReadAsStringAsync().Result;
				var webResult = JsonConvert.DeserializeObject<WebServiceResult>(userRest);
				if (webResult.data == null)
					return null;
				FilterCourseDTO filterCourses = JsonConvert.DeserializeObject<FilterCourseDTO>(webResult.data);
				return filterCourses;
			} catch (Exception ex) {
				throw ex;	
			}
		}	
	}
}

