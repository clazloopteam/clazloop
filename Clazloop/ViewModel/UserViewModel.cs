﻿using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text;
using PCLCrypto;
using MhanoHarkness;
using Clazloop.DTOs;

namespace Clazloop
{
	public class UserViewModel : BaseViewModel
	{
		public UserViewModel ()
		{
			
		}

		public async Task <UserRestDTO> GetUserInfo(string Email, string Password)
		{
			UserRestDTO userInfo;
			try 
			{
				userInfo = new UserRestDTO();
				string encryptedPassword = EncryptText (Password);
				if (String.IsNullOrEmpty(encryptedPassword)) return null;

				string Url = string.Format ("{0}/clazloop-service/rest/securityService/userLogin?username={1}&password={2}",App.CLAZLOOP_DOMAIN,Email, encryptedPassword);
				var client = new System.Net.Http.HttpClient ();
				var response = await client.GetAsync(Url);
				var userRest = response.Content.ReadAsStringAsync().Result;
				var webResult = JsonConvert.DeserializeObject<WebServiceResult>(userRest);
				if (webResult.data == null)
					return null;
				userInfo = JsonConvert.DeserializeObject<UserRestDTO>(webResult.data);
				return userInfo;
			} catch (Exception ex) {
				return null;	
			}
		}

		public string EncryptText (string TextPlain)
		{
			try 
			{
				byte[] keySimetric = new byte[16];
				var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha1);
				byte[] inputBytes = Encoding.UTF8.GetBytes(TextPlain);
				byte[] hash = hasher.HashData(inputBytes);
				Array.Copy (hash, keySimetric, 16);
				return Base64Url.ToBase64ForUrlString (keySimetric);
			} catch (Exception ex) {
				return null;
			}	
		}
	}
}

