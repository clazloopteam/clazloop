﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.XamarinForms.Primitives;
using Xamarin.Forms;

namespace Clazloop
{
	public partial class DashBoard : ContentPage
	{
		public DashBoard ()
		{
			InitializeComponent ();
			this.drawer.DrawerLength = 600d;
			List<MenuDTO> OpcionesMenu = new List<MenuDTO> {
				new MenuDTO ("Hola Pedro Perez", "usuario.png"),
				new MenuDTO ("Inicio", "ic_home.png"),
				new MenuDTO ("Biblioteca", "biblioteca.png"),
				new MenuDTO ("Mis Cursos", "cursos.png"),
				new MenuDTO ("Ajustes", "ajustes.png"),
				new MenuDTO ("Contactanos", "contactanos.png"),
				new MenuDTO ("Cerrar Sesión", "cerrar.png")
			};

			List<MenuDTO> Contenido = new List<MenuDTO> ();
			for (int i = 0; i < 50; i++) {
				Contenido.Add (new MenuDTO (string.Format ("Contenido {0}", i),"cursos.png"));
			}

			var cell = new DataTemplate (typeof(ImageCell));
			cell.SetBinding (TextCell.TextProperty, "Titulo");
			cell.SetBinding (ImageCell.ImageSourceProperty, "Imagen");
			this.drawerList.RowHeight = 60;
			this.drawerList.ItemTemplate = cell;
			this.drawerList.ItemsSource = OpcionesMenu;
			this.contentList.ItemsSource = Contenido;
			this.contentList.ItemTemplate = cell;
			this.Padding = new Thickness (10, Device.OnPlatform (20, 0, 0), 10, 5);

		}

		void OnToolbarButtonClick(object sender, EventArgs e)
		{
			drawer.IsOpen = !drawer.IsOpen;
		}

//		public void OnActivitySelected (object o, ItemTappedEventArgs e)
//		{
//			var activity = e.Item as Activity;
//			ContentPage page = activity.GetContentPage ();
//			Navigation.PushAsync (page);
//		}
	}
}

