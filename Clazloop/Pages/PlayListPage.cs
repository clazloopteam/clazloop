﻿using Xamarin.Forms;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class PlayListPage : MasterPage
	{
		public PlayListPage ()
		{
			Title = "Play List";
			Icon = "ajustes.png";
			this.Content = new StackLayout ();
		}
	}
}
