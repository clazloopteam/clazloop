﻿using Xamarin.Forms;
using System.Collections.ObjectModel;
using Clazloop.Controls.CourseDetail;
using Clazloop.DTOs;
using Clazloop.ListViewTemplates;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class CourseDetailOS : ContentPage
	{
		StackLayout skInner;
		VideoViewOS VideoIOS; 
		ObservableCollection<SectionDTO> courseSecuence;
		StackLayout skDescription;
		ScrollView scView;
		CourseDetailListView _list;
		StackLayout stackLayout;
		Label lblDescription;
		double VideoRelationWidth;
		double VideoRelationHigth;

		public CourseDetailOS (CourseDetailDTO course)
		{
			VideoRelationWidth = App.ScreenWidth / 2.5;
			VideoRelationHigth = App.ScreenHeight / 3.2;

			skInner = new StackLayout()
			{
				Padding = new Thickness(0,0,0,0),
				WidthRequest = VideoRelationWidth,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = VideoRelationHigth,
				Orientation = StackOrientation.Vertical,
				BackgroundColor = Color.White
			};
			VideoIOS = new VideoViewOS();
            skInner.Children.Add(VideoIOS);
			courseSecuence = course.GetSectionList();

			SectionDTO nextVideo = course.FindVideoToView ();
			if (nextVideo != null) SetVideoContent (nextVideo);
			var skTitle = new TitleCourse(course.Name);		
			var skAuthor = new AuthorCourse(course.Authors[0].Name);

			lblDescription = new Label () { 
				Style = AppStyle.AuthorTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				Text = course.GetDescription,
			};

		    skDescription = new StackLayout{ 
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(5,3),
				Orientation = StackOrientation.Vertical,
				IsVisible = true
			};
			skDescription.Children.Add (lblDescription);

			Controls.CourseDetail.ProgressBar ctrlProgressBar = new Controls.CourseDetail.ProgressBar(AppStyle.LightGrey, course.Progress);

			var lblProgreso = new Label () {
				Style = AppStyle.CourseTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				Text = string.Format ("{0} % AVANCE DEL CURSO", course.Progress.ToString ()), 
				HorizontalTextAlignment = TextAlignment.End,
				VerticalTextAlignment = TextAlignment.Center
			};

			var skProgress = new StackLayout () {
				HeightRequest = 30,
				Orientation = StackOrientation.Horizontal,
				Children = {ctrlProgressBar, lblProgreso}
			};

			var skOuterProgress = new StackLayout () {
				HeightRequest = 30,
				BackgroundColor = AppStyle.LightGrey,
				Orientation = StackOrientation.Vertical,
				Children = {skProgress, new BoxView () { Color = Color.White, HeightRequest = 0.5, Opacity = 0.5 }}
			};

			stackLayout = new StackLayout
			{
				Spacing = 0,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Vertical,
				BackgroundColor = AppStyle.LightGrey,
				Children = 
				{
					skTitle,
					skAuthor,
					new StackLayout
					{
						Spacing = 0,
						Orientation = StackOrientation.Horizontal,
						BackgroundColor = AppStyle.LightGrey,
						Padding = new Thickness(5,3),
						Children = 
						{
							//TODO: Fecha de Curso
							new Label () { 
								Style = AppStyle.AuthorTitleStyle,
								FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
								VerticalOptions = LayoutOptions.Center,
								Text = "Enero 30, 2015"
							},
							//TODO: Nivel
							new Label () { 
								Style = AppStyle.DurationTitleStyle,
								FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
								VerticalOptions = LayoutOptions.Center,
								HorizontalOptions = LayoutOptions.CenterAndExpand,
								Text = course.Level.Name
							},
							//TODO: Duracion del Curso
							new Label () { 
								Style = AppStyle.DurationTitleStyle,
								FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
								VerticalOptions = LayoutOptions.Center,
								Text = course.TotalTime
							}
						}
					},
					skDescription,
					skOuterProgress,
				}
			};	

			_list = new CourseDetailListView ();
			_list.VerticalOptions = LayoutOptions.FillAndExpand;
			_list.HorizontalOptions = LayoutOptions.FillAndExpand;
			_list.ItemsSource = courseSecuence;
			_list.ItemTemplate = new DataTemplate (typeof(SectionDetailLinealOS));
			_list.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) => {
				if (e.SelectedItem == null)  return;
				if (((SectionDTO)e.SelectedItem).IsSuggest){
					VideoIOS.PlayerAction = VideoState.STOP;
					this.IsBusy = true;
					CourseViewModel curso = new CourseViewModel();
					CourseDetailDTO courseDetail = await curso.GetCourseInfo (App.UserInfo.Email, App.UserInfo.Token, (short) ((SectionDTO)e.SelectedItem).Id);
					if (courseDetail == null) {
						await DisplayAlert("Esto es embarazoso","Ups, ocurrio un error. Intenta más tarde","Ok");
						this.IsBusy = false;
						return;
					};
					await Navigation.PushAsync (new CourseDetailOS(courseDetail));
					this.IsBusy = false;
					return;
				}
				if (((SectionDTO)e.SelectedItem).IsTitle) {
					_list.SelectedItem = null;
					return;
				}
				SetVideoContent((SectionDTO)e.SelectedItem);
			};
			stackLayout.Children.Add (_list);
		    scView = new ScrollView () {
				VerticalOptions = LayoutOptions.FillAndExpand,
				IsClippedToBounds = true,
				Orientation = ScrollOrientation.Vertical        
			};
			scView.Content = stackLayout;
			var skOuter = new StackLayout (){  Padding = 0, Orientation = StackOrientation.Vertical };
			skOuter.Children.Add (skInner);
			skOuter.Children.Add (scView);
			this.Content = skOuter; 
		}

		void SetVideoContent (SectionDTO section)
		{
            skInner.HorizontalOptions = LayoutOptions.FillAndExpand;
			skInner.HeightRequest = VideoRelationHigth;
            VideoIOS.HorizontalOptions = LayoutOptions.FillAndExpand;
			VideoIOS.HeightRequest = VideoRelationHigth;
			VideoIOS.VideoSource = section.FullVideoUrl;

		}
	}
}


