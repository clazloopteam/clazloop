﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using Telerik.XamarinForms.Common;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.Primitives.SideDrawer;
using Telerik.XamarinForms.Primitives;
using ImageCircle.Forms.Plugin.Abstractions;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections.Specialized;

namespace Clazloop
{
	public class CourseCardView : ListViewTemplateCell
	{
		public CourseCardView()
		{
			Image imgCourse = new Image ();
			imgCourse.Aspect = Aspect.Fill;
			imgCourse.WidthRequest = 180;
			imgCourse.HeightRequest = 90;
			imgCourse.Source = new UriImageSource {
				CachingEnabled = true,
				CacheValidity = new TimeSpan(5,0,0,0),
			};
			imgCourse.SetBinding (Image.SourceProperty,"FullUrlImage");

			var skImage = new StackLayout {
				Padding = 0,
				WidthRequest = 180,
				HeightRequest = 90,
				Children = {imgCourse}
			};

			var lblNombre = new Label () {	
				Style = AppStyle.CourseTitleStyle,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};
			lblNombre.SetBinding (Label.TextProperty, "Named");

			var skNombre = new StackLayout{ 
				Orientation = StackOrientation.Horizontal
			};
			skNombre.Children.Add (lblNombre);

			var lblAutor = new Label () {
				Style = AppStyle.AuthorTitleStyle,
			};
			lblAutor.SetBinding (Label.TextProperty, "AuthorFullName");

			var lblDuracion = new Label () { 
				Style = AppStyle.DurationTitleStyle	
			}; 
			lblDuracion.SetBinding (Label.TextProperty, "TotalTime");

			var imgClock = new Image () {
				Source = "ic_clock.png",
				HeightRequest = 12,
				WidthRequest = 12
			};

			var lblNivel = new Label () { 
				Style = AppStyle.DurationTitleStyle,
				Text = "Basico"
			};
			//lblNivel.SetBinding (Label.TextProperty, "Level");

			var imgNivel = new Image () {
				Source = "ic_level.png",
				HeightRequest = 12,
				WidthRequest = 12
			};

			var stkDuracion = new StackLayout () {
				Padding = new Thickness(0,0,0,0),
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = { imgClock, lblDuracion, imgNivel, lblNivel }
			};

			var skDescripcion = new StackLayout {
				Padding = new Thickness(5,0,5,0),
				Orientation = StackOrientation.Vertical,
				Spacing = 0,
				Children = {skNombre, lblAutor, stkDuracion}		
			};

			var skDetalles = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Children = { skImage, skDescripcion }
			};

			var cellLayout = new StackLayout {
				Padding = 2d,
				BackgroundColor = Color.Transparent,
				WidthRequest = 180,
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				Children = { skDetalles }
			};
			this.View = cellLayout;
		}
	}


	public class PlayListCardView : ListViewTemplateCell
	{
		public PlayListCardView()
		{
			Image imgCourse = new Image ();
			imgCourse.Aspect = Aspect.Fill;
			imgCourse.WidthRequest = 180;
			imgCourse.HeightRequest = 112;
			imgCourse.Source = new UriImageSource {
				CachingEnabled = true,
				CacheValidity = new TimeSpan(5,0,0,0),
			};
			imgCourse.Source = ImageSource.FromFile ("ic_playlist_folder.png");

			var skImage = new StackLayout {
				Padding = 0,
				WidthRequest = 192,
				HeightRequest = 112,
				Children = {imgCourse}
			};

			var lblNombre = new Label () {	
				Style = AppStyle.CourseTitleStyle,
				LineBreakMode = LineBreakMode.CharacterWrap
			};
			lblNombre.SetBinding (Label.TextProperty, "Name");

			var lblCantidadCursos = new Label () { 
				Style = AppStyle.AuthorTitleStyle,	
				FontAttributes = FontAttributes.Bold
			};
			lblCantidadCursos.SetBinding (Label.TextProperty, "GetCourseNumber");

			var skDescripcion = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Spacing = 0,
				Padding = 10,
				Children = {lblNombre, lblCantidadCursos}		
			};

			var skDetalles = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Children = { skImage, skDescripcion }
			};

			var cellLayout = new StackLayout {
				Padding = 2d,
				BackgroundColor = Color.Transparent,
				WidthRequest = 180,
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				Children = { skDetalles }
			};
			this.View = cellLayout;
		}
	}

	public class CourseViewCellLineal : Telerik.XamarinForms.DataControls.ListView.ListViewTemplateCell
	{
		public CourseViewCellLineal()
		{
			Image imgCourse = new Image ();
			imgCourse.BackgroundColor = Color.Aqua;
			imgCourse.SetBinding (Image.SourceProperty, "FullUrlImage");
			imgCourse.HeightRequest = 100;
			imgCourse.WidthRequest = 140;
			imgCourse.Aspect = Aspect.Fill;

			var frmImagen = new Frame {
				BackgroundColor = Color.Transparent,
				OutlineColor = Color.Gray,
				Padding = 2
			};
			frmImagen.Content = imgCourse;

			var lblNombre = new Label () {
				Style = AppStyle.CourseTitleLinearStyle
			};
			lblNombre.SetBinding (Label.TextProperty, "Named");
			var skNombre = new StackLayout {
				Orientation = StackOrientation.Horizontal
			};
			skNombre.Children.Add (lblNombre);

			var lblAutor = new Label () {
				Style = AppStyle.AuthorTitleStyle
			};
			lblAutor.SetBinding (Label.TextProperty, "AuthorFullName");


			var lblDuracion = new Label () { 
				Style = AppStyle.DurationTitleStyle
			};
			lblDuracion.SetBinding (Label.TextProperty, "TotalTime");

			var imgClock = new Image () {
				Source = "ic_clock.png",
				HeightRequest = 12,
				WidthRequest = 12
			};

			var lblNivel = new Label () { 
				Style = AppStyle.DurationTitleStyle
			};
			lblNivel.SetBinding (Label.TextProperty, "LevelName");

			var imgNivel = new Image () {
				Source = "ic_level.png",
				HeightRequest = 12,
				WidthRequest = 12
			};

			var stkDuracion = new StackLayout () {
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = { imgClock, lblDuracion, imgNivel, lblNivel }
			};

			var skDetalles = new StackLayout {
				Padding = 0,
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { skNombre, lblAutor, stkDuracion }
			};

			var skInnerCell = new StackLayout {
				Padding = 10,
				Orientation = StackOrientation.Horizontal,
				Children = { frmImagen, skDetalles  }
			};
			this.View = skInnerCell;
		}
	}

	public class IntegerConverter : IValueConverter
	{
		public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value is Int64)
				return value.ToString ();
			return value;
		}

		public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			Int64 entero;
			if (Int64.TryParse (value as string, out entero)) return entero;
			return value;
		}
	}
}
