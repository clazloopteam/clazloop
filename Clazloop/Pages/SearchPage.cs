﻿using System;
using Xamarin.Forms;
using XLabs.Forms.Controls;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class SearchPage: MasterPage
	{
		public SearchPage ()
		{
			Title = "Busqueda";
			var imgTitulo = new Image {Source = AppStyle.Icons.BannerTop};
			imgTitulo.Aspect = Aspect.AspectFit;

			var imgSubTitulo = new Image {Source = AppStyle.Icons.BannerBottom};
			imgSubTitulo.Aspect = Aspect.AspectFit;

			var btnTipo = new Switch {
				HorizontalOptions = LayoutOptions.Center
			};
		
			var txtBusqueda = new ExtendedEntry  {
				Keyboard = Keyboard.Default,
				PlaceholderTextColor = AppStyle.DarkBlue,
				TextColor = AppStyle.DarkBlue,
				WidthRequest = 600,
				XAlign = TextAlignment.Center
			};

			SearchBar shBarra = new SearchBar {
				Placeholder = "Búsqueda por Curso, Autor o Palabra Clave",
				WidthRequest = 600
			};

			shBarra.SearchButtonPressed += (object sender, EventArgs e) => {
				Navigation.PushAsync (new ResultPage(shBarra.Text));
			};

			var skLayout = new StackLayout {
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Children = {imgTitulo,	imgSubTitulo, shBarra}
			};

			var bv = new BoxView();
			bv.Color = Color.White;
			bv.InputTransparent = true;
		 
			var stackLayoutWrapped = new ContentView
			{
				Content = skLayout
			};
			var layout = new AbsoluteLayout();
			layout.Children.Add(bv, new Rectangle(0d,0d,1d,1d), AbsoluteLayoutFlags.All);
			layout.Children.Add(stackLayoutWrapped, new Rectangle(0d, 0d, 1d, 1d), AbsoluteLayoutFlags.All);
		    this.Content = layout;	
		}	
	}
}

