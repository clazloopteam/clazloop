﻿using Xamarin.Forms;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class ContactoPage : MasterPage
	{
		public ContactoPage ()
		{

			Title = "Contactanos";
			Icon = "contacto.png";

			Padding = new Thickness (10, 10 , 10, 10);
			BackgroundColor = AppStyle.BlueBackgroundColor;

			var layout = new StackLayout {
				Padding = new Thickness(20,20,20,20),
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				BackgroundColor = Color.White,
			};

			Label lblTitle = new Label {
				FontFamily = AppStyle.FontFamily,
				Text = "Asunto",
				TextColor = AppStyle.LightBlue,
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				MinimumHeightRequest = 5
			};

			Entry etAsunto = new Entry {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				MinimumHeightRequest=20,
				Keyboard = Keyboard.Text,
				BackgroundColor = Color.White
			};

	
			Label lblTo = new Label {
				FontFamily = AppStyle.FontFamily,
				Text = "Para ",
				TextColor = AppStyle.LightBlue,
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
			};

			Label lblDesCorreo = new Label {
				FontFamily = AppStyle.FontFamily,
				Text = "soporte@clazloop.com",
				TextColor = AppStyle.Grey,
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
			};

			Label lblMensaje = new Label {
				FontFamily = AppStyle.FontFamily,
				Text = "Mensaje",
				TextColor = AppStyle.LightBlue,
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				MinimumHeightRequest = 5
			};
					
			Editor etMensaje = new Editor {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Text = "",
				Keyboard = Keyboard.Chat,
				BackgroundColor = Color.White,
				MinimumHeightRequest = 50
			};

			var gridLayoutBottoms = new StackLayout {
				Padding =  new Thickness(0,20,0,0), 
				// = LayoutAlignment.Center,
				Spacing = 10,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Center
			};
				

			Button btnEnviar = new Button {
				Text = "Enviar",
				WidthRequest = 75,
				HeightRequest = 30,
				TextColor =Color.White,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor =  AppStyle.Orange,
				FontSize = 12
			};


			btnEnviar.Clicked += async (object sender, System.EventArgs e) => {
				if (!(Device.OS == TargetPlatform.iOS)) {
					string msjResult = DependencyService.Get<ISendEmail>().SendEmail(etAsunto.Text,etMensaje.Text);
					await DisplayAlert("Clazloop",msjResult,"OK");
				}else{
					await DisplayAlert("Clazloop","Su mensaje ha sido enviado.En breve nos comunicaremos con usted","OK");
				}
				lblTitle.Text = string.Empty;
				lblMensaje.Text = string.Empty;

			};


			Button btnCancelar = new Button {
				Text = "Cancelar",
				WidthRequest = 75,
				HeightRequest = 15,
				TextColor =Color.White,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor =  AppStyle.BlueBackgroundColor,
				FontSize = 12
			};

			btnCancelar.Clicked += async (object sender, System.EventArgs e) => {
				if (Device.OS == TargetPlatform.iOS)
				{
					Application.Current.MainPage = new NavigationPage(new RootPage());
				}
				else {
					Application.Current.MainPage = new RootPage();
				}
			};



			gridLayoutBottoms.Children.Add (btnEnviar);
			gridLayoutBottoms.Children.Add (btnCancelar);

			layout.Children.Add (lblTo);
			layout.Children.Add (lblDesCorreo);
			layout.Children.Add (lblTitle);
			layout.Children.Add (etAsunto);
			layout.Children.Add (lblMensaje);
			layout.Children.Add (etMensaje);
			layout.Children.Add (gridLayoutBottoms);

			Content = layout;
		}
	}
}
