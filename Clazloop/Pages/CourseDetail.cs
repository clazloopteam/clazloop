﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using DeviceOrientation.Forms.Plugin.Abstractions;
using Clazloop.Controls.CourseDetail;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class CourseDetail: MasterPage
	{
		StackLayout skInner;
		StackLayout skSections;
		ScrollView scView;
		CrossVideoPlayerView cp;
		List<SectionDTO> courseSecuence;
		SectionDTO sSection;
		string sCourseName;

		IDeviceOrientation _deviceOrientationSvc;

		public CourseDetail (CourseDetailDTO course)
		{
			_deviceOrientationSvc = DependencyService.Get<IDeviceOrientation> ();

			courseSecuence = course.GetCourseList;
			StackLayout skPrincipal = new StackLayout {
			};

			var skTitle = new TitleCourse( course.Name);

			var skCourseInfo = new StackLayout (){ 
				Orientation = StackOrientation.Vertical,
				BackgroundColor = AppStyle.LightGrey            
			};
			skCourseInfo.Children.Add (skTitle);

			var skAuthor = new AuthorCourse(course.Authors[0].Name);
			skCourseInfo.Children.Add (skAuthor);

			var grid = new Grid {
				Padding = new Thickness(5),
				BackgroundColor = AppStyle.LightGrey,
				ColumnSpacing = 0,
				RowSpacing = 0,
				RowDefinitions = { new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) } },
				ColumnDefinitions = {
					new ColumnDefinition { Width = new GridLength(30, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(30, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(30, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(10, GridUnitType.Star) }
				}
			};

			var skDateCourse = new DateCourse("Enero 30, 2015"); 
			var skLevel = new LevelCourse (course.Level.Name); 
			var skDuration = new DurationCourse (course.TotalTime); 

			grid.Children.Add (skDateCourse, 0, 0);
			grid.Children.Add (skLevel, 1, 0);
			grid.Children.Add (skDuration,  2, 0);

			var imgTapRight = new Image {Source = AppStyle.Icons.TapRigth, IsVisible = true, HorizontalOptions = LayoutOptions.End};  

			var skTapImage = new StackLayout () {
				BackgroundColor = AppStyle.LightGrey,
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.End,
				Children = { imgTapRight }
			};
			grid.Children.Add (skTapImage,  3, 0);
			skCourseInfo.Children.Add (grid);

			skPrincipal.Children.Add (skCourseInfo);

			var lblDescription = new Label () { 
				Style = AppStyle.AuthorTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				Text = course.GetDescription,
			};

			var skDescription = new StackLayout{ 
				Padding = 10,
				Orientation = StackOrientation.Vertical,
				IsVisible = false
			};
			skDescription.Children.Add (lblDescription);
			skPrincipal.Children.Add (skDescription); 

			var ftGesture1 = new TapGestureRecognizer ();
			ftGesture1.Tapped += async (object sender, EventArgs e) => {
				skDescription.IsVisible = !skDescription.IsVisible;
				await imgTapRight.RotateTo(skDescription.IsVisible ? 90 : 0, 400);
			};
			skTapImage.GestureRecognizers.Add (ftGesture1);

			Controls.CourseDetail.ProgressBar ctrlProgressBar = new Controls.CourseDetail.ProgressBar(AppStyle.LightGrey,course.Progress);
			var skProgressBar = new StackLayout () {
				Orientation = StackOrientation.Horizontal,
				Children = { ctrlProgressBar }
			};

			var lblProgress = new Label () { 
				Style = AppStyle.CourseTitleStyle,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				Text = string.Format("{0}% AVANCE DEL CURSO",course.Progress.ToString()), 
				XAlign = TextAlignment.End,
				YAlign = TextAlignment.Center
			};

			var skProgress = new StackLayout () {
				Orientation = StackOrientation.Horizontal,
				Children = {skProgressBar, lblProgress}
			};

			var skOuterProgress = new StackLayout () {
				BackgroundColor = AppStyle.LightGrey,
				Orientation = StackOrientation.Vertical,
				Children = {skProgress, new BoxView () { Color = Color.White, HeightRequest = 0.5, Opacity = 0.5 }}
			};
			skPrincipal.Children.Add (skOuterProgress); 


			skSections = new StackLayout (){ Orientation = StackOrientation.Vertical };
			foreach (GroupSectionDTO item in course.GroupSections) {
				var skSection = new GroupSection (item.GroupName ,true, item.Sections,AppStyle.LightBlue, AppStyle.BlueBackgroundColor);
				skSection.SelectedSectionChanged += (object sender, SectionDTO e) => {
					if (e == null) return;
					SectionDTO selectedVideo = course.FindVideoByVideoNumber (e.VideoNumber);
					SetVideoContent (selectedVideo, course.Name);
				};
				skSections.Children.Add (skSection);
			}
			skPrincipal.Children.Add (skSections);

			var skSuggestion = new SuggestionSection ("Cursos Recomendados",true, course.Suggestions,AppStyle.DarkBlue,AppStyle.DarkBlue);
			skPrincipal.Children.Add (skSuggestion);

			skInner = new StackLayout (){ Orientation = StackOrientation.Vertical };
			SectionDTO nextVideo = course.FindVideoToView ();
			if (nextVideo != null) {
				SetVideoContent (nextVideo, course.Name);
			}

			scView = new ScrollView () {
				Orientation = ScrollOrientation.Vertical        
			};
			scView.Content = skPrincipal;
			var skOuter = new StackLayout (){ Orientation = StackOrientation.Vertical };
			skOuter.Children.Add (skInner);
			skOuter.Children.Add (scView);
			this.Content = skOuter; 
		}

		void SetFullScreenFromVertical()
		{
			skInner.HorizontalOptions = LayoutOptions.Fill;
			skInner.VerticalOptions = LayoutOptions.Fill;
			skInner.Orientation = StackOrientation.Vertical;
			scView.VerticalOptions = LayoutOptions.End;
			scView.HorizontalOptions = LayoutOptions.End;
			scView.IsVisible = false;
		}

		void SetVideoContent (SectionDTO section, string courseName)
		{
			foreach (var item in skSections.Children) {
				var aux = (List<SectionDTO>)((GroupSection)item).ListControl.ItemsSource;
				((GroupSection)item).ListControl.SelectedItem = !aux.Contains (section) ? null : section;
			}
			if (section == null) return;
			if (skInner.Children.Count != 0) skInner.Children.RemoveAt(0);
			cp = new CrossVideoPlayerView ();
			cp.VideoScale = 1.77;
			cp.IsVisible = true;
			cp.VideoName = section.VideoName;
			cp.Name = section.Name;
			cp.VideoSource = section.FullVideoUrl;
			cp.HorizontalOptions = LayoutOptions.FillAndExpand;
			cp.VerticalOptions = LayoutOptions.FillAndExpand;
			cp.onVideoViewClick += new VideoViewDelegate (SetFullScreenFromVertical);
			this.Title = courseName;
			sSection = section;
			sCourseName = courseName;
			skInner.Children.Add (cp);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			MessagingCenter.Subscribe<DeviceOrientationChangeMessage>(this, DeviceOrientationChangeMessage.MessageId, (message) =>
				{
					HandleOrientationChange(message);
				});
		}

		protected override void OnDisappearing()
		{
			MessagingCenter.Unsubscribe<DeviceOrientationChangeMessage>(this, DeviceOrientationChangeMessage.MessageId);
			base.OnDisappearing();
		}

		private void HandleOrientationChange(DeviceOrientationChangeMessage mesage)
		{

			if (mesage.Orientation == DeviceOrientation.Forms.Plugin.Abstractions.DeviceOrientations.Landscape) {
				skInner.HorizontalOptions = LayoutOptions.FillAndExpand;
				skInner.VerticalOptions = LayoutOptions.FillAndExpand;
				skInner.Orientation = StackOrientation.Horizontal;
				scView.VerticalOptions = LayoutOptions.End;
				scView.HorizontalOptions = LayoutOptions.End;
				scView.IsVisible = false;
			} else {
				skInner.Orientation = StackOrientation.Vertical;
				scView.IsVisible = true;
			}
		}
	}
} 
