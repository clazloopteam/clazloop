﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.Common;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections.ObjectModel;
using Clazloop.DTOs;
using Clazloop.ListViewTemplates;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class FilterPage: ContentPage
	{
		RadListView _listViewAuthor;
		RadListView _listViewLevel;
		RadListView _listViewSubCategory;

		public FilterPage (EnumTipoFiltro TipoFiltro)
		{
			try {			
				Icon = "ic_filter.png";
				var layout = new StackLayout {
					Spacing = 0,
					VerticalOptions = LayoutOptions.FillAndExpand
				};
				switch (TipoFiltro)
				{
				case EnumTipoFiltro.INSTRUCTOR:
						Title = string.Format ("Filtrar por Instructor - {0}", App.FiltroTematica.Name);
						_listViewAuthor = new RadListView ();
						_listViewAuthor.ItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
						_listViewAuthor.SelectedItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
						_listViewAuthor.SelectionGesture = SelectionGesture.Tap;
						_listViewAuthor.SelectionMode = SelectionMode.Single;
						_listViewAuthor.VerticalOptions = LayoutOptions.FillAndExpand;
						_listViewAuthor.HorizontalOptions = LayoutOptions.FillAndExpand;
						_listViewAuthor.ItemsSource = App.FiltroTematicaAutor;
						_listViewAuthor.ItemTemplate = new DataTemplate (typeof(AuthorViewCellLineal)); 
						_listViewAuthor.GroupDescriptors.Add (new PropertyGroupDescriptor { PropertyName = "Letter" });
						var descriptor = new PropertySortDescriptor { PropertyName = "Letter", SortOrder = SortOrder.Ascending };
						_listViewAuthor.SortDescriptors.Add (descriptor);
						_listViewAuthor.ItemTapped += (object sender, ItemTapEventArgs e) => {
							App.FiltroAutor = new AuthorRestDTO(((AuthorRestDTO)e.Item).Id,((AuthorRestDTO)e.Item).Name/*, ((AuthorRestDTO)e.Item).UrlImage*/);
							Navigation. PopAsync();
						};
						layout.Children.Add (_listViewAuthor);	
						break;
				case EnumTipoFiltro.NIVEL:
						Title = string.Format("Filtrar por Nivel - {0}",App.FiltroTematica.Name);
						_listViewLevel = new RadListView ();
						_listViewLevel.ItemStyle = AppStyle.ListViewLinearLayouBottomBorder;;
						_listViewLevel.SelectedItemStyle = AppStyle.ListViewLinearLayouBottomBorder;;
						_listViewLevel.SelectionGesture = SelectionGesture.Tap;
						_listViewLevel.SelectionMode = SelectionMode.Single;
						_listViewLevel.VerticalOptions = LayoutOptions.FillAndExpand;
						_listViewLevel.HorizontalOptions = LayoutOptions.FillAndExpand;
						ObservableCollection<LevelRestDTO> levelList = new ObservableCollection<LevelRestDTO> () {
							new LevelRestDTO (1, "Basico"),
							new LevelRestDTO (2, "Intermedio"),
							new LevelRestDTO (3, "Avanzado")
						};
						_listViewLevel.ItemsSource = levelList;
						_listViewLevel.ItemTemplate = new DataTemplate (typeof(LevelViewCell)); 
						_listViewLevel.ItemTapped += (object sender, ItemTapEventArgs e) => {
							App.FiltroNivel = (LevelRestDTO)e.Item;
							Navigation.PopAsync();
						};
						layout.Children.Add (_listViewLevel);
						break;
				case EnumTipoFiltro.TEMATICA:
					Title = string.Format ("Filtrar por Temáticas - {0}", App.FiltroTematica.Name);
					_listViewSubCategory = new RadListView ();
					_listViewSubCategory.ItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
					_listViewSubCategory.SelectedItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
					_listViewSubCategory.SelectionGesture = SelectionGesture.Tap;
					_listViewSubCategory.SelectionMode = SelectionMode.Single;
					_listViewSubCategory.VerticalOptions = LayoutOptions.FillAndExpand;
					_listViewSubCategory.HorizontalOptions = LayoutOptions.FillAndExpand;
					_listViewSubCategory.ItemsSource = App.FiltroSubTematica;
					_listViewSubCategory.ItemTemplate = new DataTemplate (typeof(ThematicViewCell));
					_listViewSubCategory.SelectionMode = SelectionMode.None;
					_listViewSubCategory.LayoutDefinition.ItemLength = App.ScreenHeight / 10;
					_listViewSubCategory.ItemTapped += (object sender, ItemTapEventArgs e) => {
							App.FiltroTematica = (ThematicDTO)e.Item;
							Navigation.PopAsync();
					};
					layout.Children.Add (_listViewSubCategory);
					break;
				}
				Content = layout;
			} catch (Exception ex) {
				throw ex;
			}
		}
	}
}

