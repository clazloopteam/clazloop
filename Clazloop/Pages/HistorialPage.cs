﻿using Xamarin.Forms;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using Clazloop.ListViewTemplates;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class HistorialPage : MasterPage
	{
		RadListView _listHistory;

		public HistorialPage ()
		{
			Title = "Historial";
			Icon = "ic_Home";
			_listHistory = new RadListView ();
			_listHistory.LayoutDefinition.ItemLength = 120;
			_listHistory.SelectionGesture = SelectionGesture.Tap;
			_listHistory.SelectionMode = SelectionMode.Single;
			_listHistory.VerticalOptions = LayoutOptions.FillAndExpand;
			_listHistory.HorizontalOptions = LayoutOptions.FillAndExpand;
			_listHistory.ItemsSource = App.UserInfo.coursesHistory;
			_listHistory.ItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
			_listHistory.SelectedItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
			_listHistory.ItemTemplate = new DataTemplate (typeof(CourseViewCellLineal));
			_listHistory.SelectionChanged += async (sender, e) => {
				if (((RadListView)sender).SelectedItems.Count == 0)
					return;
				var itemSelected = (CourseRestDTO)((RadListView)sender).SelectedItems [0];
				this.IsBusy = true;
				CourseViewModel curso = new CourseViewModel();
				CourseDetailDTO courseDetail = await curso.GetCourseInfo (App.UserInfo.Email, App.UserInfo.Token, itemSelected.Id);
				if (courseDetail == null) {
					await DisplayAlert("Esto es embarazoso","Ups, ocurrio un error. Intenta más tarde","Ok");
					this.IsBusy = false;
					return;
				};
				Navigation.PushAsync (new CourseDetail (courseDetail));
				this.IsBusy = false;
			};
			var _historylayout = new StackLayout {
				Spacing = 0,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			_historylayout.Children.Add (_listHistory);
			this.Content = _historylayout;
		}
	}
}

