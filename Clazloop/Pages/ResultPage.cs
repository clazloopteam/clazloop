﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using Clazloop.ListViewTemplates;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class ResultPage : MasterPage
	{
		SearchBar shBarra;
		RadListView listView;

		public ResultPage (string CadenaBuscar)
		{
			Title = string.Format ("Resultados de Búsqueda -> {0}",CadenaBuscar);
			shBarra = new SearchBar () {
				Placeholder = "Búsqueda por Curso, Autor o Palabra Clave",
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
				CancelButtonColor = Color.Red,
				IsVisible = false, 
			};
			shBarra.SearchButtonPressed += (object sender, EventArgs e) => {
				CadenaBuscar = shBarra.Text;
				Cargar(CadenaBuscar);
				listView.ItemTemplate = new DataTemplate (typeof(CourseViewCellLineal));
				shBarra.IsVisible = !shBarra.IsVisible;
			};

			ToolbarItems.Add(new ToolbarItem("", "search_icon.png", () =>
			{
				shBarra.IsVisible = !shBarra.IsVisible;
			}));

			listView = new RadListView ();		
			var layout = new StackLayout {
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			Cargar(CadenaBuscar);
			listView.ItemTemplate = new DataTemplate (typeof(CourseViewCellLineal));
			layout.Children.Add (shBarra);	
			layout.Children.Add (listView);	
			Content = layout;
		}

		async void Cargar(string CadenaBuscar)
		{
			this.IsBusy = true;
			FilterCourseViewModel FilterCourse = new FilterCourseViewModel();
			var listAux = await FilterCourse.GetCourses(App.UserInfo.Email,App.UserInfo.Token,0,0,0,CadenaBuscar,0);
			if (listAux == null) {
				DisplayAlert("Información","No existen datos con tus criterios de búsqueda.","Ok");
				this.IsBusy = false;
				shBarra.Text = string.Empty;
				return;
			};
			this.IsBusy = false;
			listView.ItemsSource = listAux.Cursos;
			listView.SelectionGesture = SelectionGesture.Tap;
			listView.SelectionMode = SelectionMode.Single;
			listView.VerticalOptions = LayoutOptions.FillAndExpand;
			listView.HorizontalOptions = LayoutOptions.FillAndExpand;
			listView.LayoutDefinition.VerticalItemSpacing = 5;
		}
	}
}

