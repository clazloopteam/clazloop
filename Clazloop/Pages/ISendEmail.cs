 using Xamarin.Forms;
using System.Net.Mail;

namespace Clazloop
{
	public interface ISendEmail
	{
		string SendEmail (string title, string body);
		void CallFormSendEmail();
	}
}
