﻿using Xamarin.Forms;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class CursosPage : MasterPage
	{
		public CursosPage ()
		{
			Title = "Cursos";
			Icon = "cursos.png";
		}
	}
}
