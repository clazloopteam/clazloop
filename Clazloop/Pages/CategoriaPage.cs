﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections.Specialized;
using Clazloop.ListViewTemplates;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class CategoriaPage : MasterPage
	{
		RadListView _listView;
		private int Page = 1;

		public CategoriaPage ()
		{
			this.Title = App.FiltroTematica.Name;
			var ftGesture1 = new TapGestureRecognizer ();
			ftGesture1.Tapped += (object sender, EventArgs e) => {
				this.IsBusy = true;
				Navigation.PushAsync (new FilterPage (EnumTipoFiltro.TEMATICA));
				this.IsBusy = false;
			};

			var ftGesture2 = new TapGestureRecognizer ();
			ftGesture2.Tapped += (object sender, EventArgs e) => {
				this.IsBusy = true;
				Navigation.PushAsync (new FilterPage (EnumTipoFiltro.INSTRUCTOR));
				this.IsBusy = false;
			};

			var ftGesture3 = new TapGestureRecognizer ();
			ftGesture3.Tapped += (object sender, EventArgs e) => {
				this.IsBusy = true;
				Navigation.PushAsync (new FilterPage (EnumTipoFiltro.NIVEL));
				this.IsBusy = false;
			};
				
			var imgTap1 = new Image {BackgroundColor = AppStyle.FilterBackgroundColor, Source = AppStyle.Icons.Tap};  
			var imgTap2 = new Image {BackgroundColor = AppStyle.FilterBackgroundColor, Source = AppStyle.Icons.Tap};  
			var imgTap3 = new Image {BackgroundColor = AppStyle.FilterBackgroundColor, Source = AppStyle.Icons.Tap};  

			var skTematica = new StackLayout {
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 20
			};

			var skInstructor = new StackLayout {
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 20
			};

			var skNivel = new StackLayout {
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 20
			};

			Label lblTematica = new Label {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				BackgroundColor = AppStyle.FilterBackgroundColor,
				Text = "Filtrar por Temática",
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.White,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
			};
			skTematica.Children.Add (lblTematica);
			skTematica.Children.Add (imgTap1);
			var skOuterTematica = new StackLayout { 
				Orientation = StackOrientation.Vertical,
				Children = { skTematica, new BoxView() {Color = Color.White, HeightRequest = 1.5, Opacity = 0.5  } },
				IsVisible = false
			};

			Label lblInstructor = new Label {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				BackgroundColor = AppStyle.FilterBackgroundColor,
				Text = "Filtrar por Instructor",
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.White,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center
			};
			skInstructor.Children.Add (lblInstructor);
			skInstructor.Children.Add (imgTap2);
			var skOuterInstructor = new StackLayout { 
				Orientation = StackOrientation.Vertical,
				Children = {skInstructor, new BoxView() {Color = Color.White, HeightRequest = 1.5, Opacity = 0.5  }},
				IsVisible = false
			};

			Label lblNivel = new Label {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				BackgroundColor = AppStyle.FilterBackgroundColor,
				Text = "Filtrar por Nivel",
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.White,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center
			};
			skNivel.Children.Add (lblNivel);
			skNivel.Children.Add (imgTap3);
			var skOuterNivel = new StackLayout { 
				Orientation = StackOrientation.Vertical,
				Children = {skNivel},
				IsVisible = false
			};

			skTematica.GestureRecognizers.Add (ftGesture1);
			skInstructor.GestureRecognizers.Add (ftGesture2);
			skNivel.GestureRecognizers.Add (ftGesture3);
				
			var skPrincipal = new StackLayout {
				Padding = 0,
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			_listView = new RadListView ();	
			_listView.SelectionGesture = SelectionGesture.Tap;
			_listView.SelectionMode = SelectionMode.Single;
			_listView.VerticalOptions = LayoutOptions.FillAndExpand;
			_listView.HorizontalOptions = LayoutOptions.FillAndExpand;
			_listView.IsPullToRefreshEnabled = true;
			_listView.RefreshRequested += this.RefreshRequested;
			RefreshRequested(this, new PullToRefreshRequestedEventArgs ());
			_listView.ItemTemplate = new DataTemplate (typeof(CourseViewCellLineal));
			_listView.LayoutDefinition.ItemLength = Device.OS == TargetPlatform.iOS ? 70 : 130;
			_listView.ItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
			_listView.SelectedItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
			_listView.SelectionChanged += async (object sender, NotifyCollectionChangedEventArgs e) => {
				if (((RadListView)sender).SelectedItems.Count == 0)	return;
				var itemSelected = (CourseRestDTOV2)((RadListView)sender).SelectedItems [0];
				this.IsBusy = true;
				CourseViewModel curso = new CourseViewModel();
				CourseDetailDTO courseDetail = await curso.GetCourseInfo (App.UserInfo.Email, App.UserInfo.Token, itemSelected.Id);
				if (courseDetail == null) {
					await DisplayAlert("Esto es embarazoso","Ups, ocurrio un error. Intenta más tarde","Ok");
					this.IsBusy = false;
					return;
				};
				if (Device.OS == TargetPlatform.Android)
				{
					await Navigation.PushAsync (new CourseDetail (courseDetail));
				} else {
					await Navigation.PushAsync (new CourseDetailOS(courseDetail));
				}
				this.IsBusy = false;
			};	
			var skFilter = new StackLayout () {
				Padding = 0,
				BackgroundColor = AppStyle.FilterBackgroundColor,
				Orientation = StackOrientation.Vertical,
				Children = {skOuterTematica, skOuterInstructor, skOuterNivel}
			};
			skPrincipal.Children.Add (skFilter);
			skPrincipal.Children.Add (_listView);	
			Content = skPrincipal;
		}

		private async void RefreshRequested(object sender, PullToRefreshRequestedEventArgs e)
		{
			try {
				this.IsBusy = true;
				FilterCourseViewModel FilterCourse = new FilterCourseViewModel();
				var listAux = await FilterCourse.GetCourses(App.UserInfo.Email,App.UserInfo.Token,
					App.FiltroTematica == null ? (Int16)0 : App.FiltroTematica.Id,
					App.FiltroAutor == null ? (Int16)0 : App.FiltroAutor.Id,
					App.FiltroNivel == null ? (Int16)0: App.FiltroNivel.Id,
					string.Empty,
					Page);	
				_listView.ItemsSource = listAux.Cursos;
				Page++;
				if (App.FiltroSubTematica == null) App.FiltroSubTematica = listAux.CategoriasHijo;
				if (App.FiltroTematicaAutor == null) App.FiltroTematicaAutor = listAux.Instructores;
				_listView.EndRefresh();
				_listView.LayoutDefinition.ItemLength = Device.OS == TargetPlatform.iOS ? 70 : 130;
				this.IsBusy = false;

			} catch (Exception ex) {
				//TODO: Cambiar por el LOG de la aplicacion. DisplayAlert ("Error", "Error recuperando información", "Ok");
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			Page = 1;
			RefreshRequested (this, new PullToRefreshRequestedEventArgs());
		}
	}
}

