﻿using Xamarin.Forms;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class AjustesPage : MasterPage
	{
		public AjustesPage ()
		{
			Title = "Ajustes";
			Icon = "ajustes.png";

			var layout = new StackLayout {
				Padding = new Thickness(20,20,20,0),
				Spacing = 20,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			Label lblContacto = new Label {
				FontFamily = AppStyle.FontFamily,
				Text = "Contacto",
				TextColor = AppStyle.LightBlue,
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
			};


			Label lblCorreo = new Label {
				Text = "\t\tsoporte@clazloop.com",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
			};

			Label lblNumero1 = new Label {
				Text = "\t\t1-888-335-9636 EE.UU (gratis)",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
			};

			Label lblNumero2 = new Label {
				Text = "\t\t1-888-335-9636 Internacional",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
			};

			Label lblDescargas = new Label {
				FontFamily = AppStyle.FontFamily,
				Text = "Descargas",
				TextColor = AppStyle.LightBlue,
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
			};

			Label lblUsoRedMovil = new Label {
				Text = "Usar Red Movil",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				IsVisible = false
			};

			Switch switcher = new Switch
			{
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				IsVisible = false
			};

			var gridLayaout = new StackLayout {
				Padding =  new Thickness(0,20,20,0), 
				Spacing = 50,
				Orientation = StackOrientation.Horizontal
			};

			Label lblMensajeRedMovil = new Label {
				Text = "Los cursos se descargaran mediante la red movil.\rDesactive esta opcion para para evitar costes de plan de datos\radicionales.",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			Label lblVersion = new Label {
				Text = "Version de la Aplicacion",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			Label lblNumeroVersion = new Label {
				Text = "4.3.2 (4.5.53)",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				XAlign = TextAlignment.End
			};

			var gridLayoutVersion = new StackLayout {
				Padding =  new Thickness(0,20,20,0), 
				Spacing = 50,
				Orientation = StackOrientation.Horizontal
			};

			Label lblDirectiva = new Label {
				Text = "Directiva de Privacidad",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			Label lblDirectivaUso = new Label {
				Text = "Directiva de Uso del Sitio Web",
				FontFamily = AppStyle.FontFamily,
				TextColor = AppStyle.DarkGrey,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			gridLayoutVersion.Children.Add (lblVersion);
			gridLayoutVersion.Children.Add (lblNumeroVersion);


			gridLayaout.Children.Add (lblUsoRedMovil);
			gridLayaout.Children.Add (switcher);

			layout.Children.Add (lblContacto);
			layout.Children.Add (lblCorreo);
			layout.Children.Add (lblNumero1);
			layout.Children.Add (lblNumero2);
			layout.Children.Add (new BoxView () { Color = AppStyle.DarkGrey, HeightRequest = 2});
			layout.Children.Add (lblDescargas);
			layout.Children.Add (gridLayaout);
			layout.Children.Add (lblMensajeRedMovil);
			layout.Children.Add (new BoxView () { Color = AppStyle.DarkGrey, HeightRequest = 2});
			layout.Children.Add (gridLayoutVersion);
			layout.Children.Add (lblDirectiva);
			layout.Children.Add (lblDirectivaUso);
			ScrollView scrollViewContent = new ScrollView { Content = layout, Orientation = ScrollOrientation.Vertical };
			Content = scrollViewContent;
		}
	}
}
