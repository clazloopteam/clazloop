﻿using Xamarin.Forms;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections;
using Clazloop.ListViewTemplates;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class CursosList : MasterPage
	{
		RadListView _listView;
		public CursosList (IList Cursos, EnumElementoDashBoard Tipo, string title = "")
		{
			switch (Tipo) {
			case EnumElementoDashBoard.HISTORIAL: 
				Title = "Historial";
				break;
			case EnumElementoDashBoard.NUEVOS:
				Title = "Nuevos Cursos";
				break;
			case EnumElementoDashBoard.PLAYLIST:
				Title = string.Format("{0}", title);
				break;
			case EnumElementoDashBoard.RECOMENDADOS:
				Title = "Cursos Recomendados";
				break;
			case EnumElementoDashBoard.FAVORITOS:
				Title = "Favoritos";
				break;
			}
			Icon = "cursos.png";
			var layout = new StackLayout {
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			_listView = new RadListView ();	
			_listView.SelectionGesture = SelectionGesture.Tap;
			_listView.SelectionMode = SelectionMode.Single;
			_listView.VerticalOptions = LayoutOptions.FillAndExpand;
			_listView.HorizontalOptions = LayoutOptions.FillAndExpand;
			_listView.ItemsSource = Cursos;
			_listView.SelectionMode = SelectionMode.Single;
			_listView.ItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
			_listView.SelectedItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
			_listView.ItemTemplate = new DataTemplate (typeof(CourseViewCellLineal));
			_listView.LayoutDefinition.ItemLength = Device.OS == TargetPlatform.iOS ? 70 : 130;
			_listView.SelectionChanged += async (sender, e) => {
				if (((RadListView)sender).SelectedItems.Count == 0)
					return;
				var itemSelected = (CourseRestDTO)((RadListView)sender).SelectedItems [0];
				this.IsBusy = true;
				CourseViewModel curso = new CourseViewModel();
				CourseDetailDTO courseDetail = await curso.GetCourseInfo (App.UserInfo.Email, App.UserInfo.Token, itemSelected.Id);
				if (courseDetail == null) {
					await DisplayAlert("Esto es embarazoso","Ups, ocurrio un error. Intenta más tarde","Ok");
					this.IsBusy = false;
					return;
				};
				if (Device.OS == TargetPlatform.Android)
				{
					await Navigation.PushAsync (new CourseDetail (courseDetail));
				} else {
					await Navigation.PushAsync (new CourseDetailOS(courseDetail));
				}
				this.IsBusy = false;
			};
			layout.Children.Add (_listView);	
			Content = layout;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
		}
	}
}
