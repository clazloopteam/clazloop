﻿using Xamarin.Forms;
using Telerik.XamarinForms.Common;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections.Specialized;
using Clazloop.DTOs;

namespace Clazloop
{
	public class BibliotecaListView : RadListView
	{
		public 	BibliotecaListView()
		{
		}
	}

	[Preserve(AllMembers=true)]
	public class BibliotecaPage : MasterPage
	{
		RadListView _listThematic;
		public BibliotecaPage ()
		{
			Title = "Biblioteca";
			Icon = "biblioteca.png";
			var layout = new StackLayout {
				Spacing = 0,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			_listThematic = new BibliotecaListView ();
			_listThematic.SelectionGesture = SelectionGesture.Tap;
			_listThematic.SelectionMode = SelectionMode.Single;
			_listThematic.VerticalOptions = LayoutOptions.FillAndExpand;
			_listThematic.HorizontalOptions = LayoutOptions.FillAndExpand;
			_listThematic.ItemsSource =  new ThematicListData ();
			_listThematic.LayoutDefinition = new ListViewLinearLayout {
				Orientation = Orientation.Vertical,
				VerticalItemSpacing = 15,
				ItemLength = App.ScreenHeight / 10
			};

			if (Device.OS == TargetPlatform.Android) {
				_listThematic.ItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
				_listThematic.SelectedItemStyle = AppStyle.ListViewLinearLayouBottomBorder;
			} else {
				
				_listThematic.ItemStyle = AppStyle.ListViewTransparentBorder;
				_listThematic.SelectedItemStyle = AppStyle.ListViewTransparentBorder;
			}
			_listThematic.ItemTemplate =  new DataTemplate (typeof(ThematicViewCell));
			_listThematic.SelectionChanged += this.SelectionChanged;
			layout.Children.Add (_listThematic);
			this.Content = layout;
		}

		private void SelectionChanged (object sender, NotifyCollectionChangedEventArgs e)
		{
			if (((RadListView)sender).SelectedItems.Count == 0)
				return;
			App.FiltroTematica = (ThematicDTO)((RadListView)sender).SelectedItems [0];
			App.Page = 0;
			App.FiltroSubTematica = null;
			App.FiltroAutor = null;
			App.FiltroNivel = null;
			this.IsBusy = true;
			Navigation.PushAsync (new CategoriaPage ());
			this.IsBusy = false;
		}
	}

	[Preserve (AllMembers = true)]
	public class ThematicViewCell : ListViewTemplateCell
	{
		public ThematicViewCell()
		{
			var lblTitulo = new Label () {
				Style = AppStyle.CourseTitleLinearStyle,
				HorizontalOptions = LayoutOptions.StartAndExpand, 
				VerticalOptions = LayoutOptions.CenterAndExpand 
			};
			lblTitulo.SetBinding (Label.TextProperty, "Name");

			Image imgCategory = new Image ();
			imgCategory.SetBinding (Image.SourceProperty, "Image");

			var tapImage = new Image () {
				Source = AppStyle.Icons.Tap,
				HorizontalOptions = LayoutOptions.End
			};

			var outerStack = new StackLayout { 
				Orientation = StackOrientation.Horizontal,
				Children = {imgCategory, lblTitulo, tapImage }
			};
			this.View = outerStack;
		}	
			
	}
}

