﻿using System;
using Xamarin.Forms;
using Clazloop.DTOs;
using System.Linq;

namespace Clazloop
{
	public partial class Login : ContentPage
	{
		public Login ()
		{
		    InitializeComponent();
			App.IsLogin = false;
			NavigationPage.SetHasNavigationBar(this, false);
			this.BackgroundColor = AppStyle.BlueBackgroundColor;

			var aiLoading = new ActivityIndicator {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				IsEnabled = true,
				IsRunning = false,
				IsVisible = true
            };

			var imgTop = new Image { 
				Source = AppStyle.Icons.Logo,
				HorizontalOptions = LayoutOptions.Center
			};

			var skTop = new StackLayout {
				BackgroundColor = AppStyle.BlueBackgroundColor,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Children = {imgTop}
			};

			var imgIniciarSesion = new Image {
				Source = AppStyle.Icons.Logo,
				HorizontalOptions = LayoutOptions.Center
			};

			var txtEmail = new Entry  {
				Keyboard = Keyboard.Email,
				Placeholder = "Correo Electrónico",
				TextColor = (Device.OS == TargetPlatform.iOS) ? Color.Black: Color.White,
				WidthRequest = 400,
				Text = "admin@clazloop.com"
			};

			var txtPassword = new Entry  {

				Keyboard = Keyboard.Default,
				Placeholder = "Password",
				TextColor = (Device.OS == TargetPlatform.iOS) ? Color.Black: Color.White,
				WidthRequest = 400,
				IsPassword = true,
				Text = "clave123"
			};

			var lblOlvido = new Label { 
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				Text = "Olvidé mi Contraseñas",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Center,
				TextColor = AppStyle.Orange, 
				IsVisible = false
			}; 
					
			var btnEntrar = new Button {
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				Text = "Entrar",
				TextColor = Color.White,
				BackgroundColor = AppStyle.Fucsia,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 302,
			};

			btnEntrar.Clicked += async (object sender, EventArgs e) => {
				if(string.IsNullOrEmpty(txtEmail.Text) && string.IsNullOrEmpty(txtPassword.Text))
				{
					await DisplayAlert("Info","Especifique Usuario y Contraseña." , "Aceptar");
					return;
				}
				var UserInfo = new UserViewModel();
				var Tematics = new ThematicViewModel();
				aiLoading.IsRunning = true;
				UserRestDTO userRest = await UserInfo.GetUserInfo(txtEmail.Text,txtPassword.Text);

				if  (userRest == null) {
					aiLoading.IsRunning = false;
					await DisplayAlert("Info","Usuario o Clave Incorrecta." , "Aceptar");
					txtEmail.Focus();
					return;
				}
				else {
					App.IsLogin = true;
					App.UserInfo = userRest;
					App.ThematicList = await Tematics.GetThematicsInfo(App.UserInfo.Email,App.UserInfo.Token);
					aiLoading.IsRunning = false;

					if (Device.OS == TargetPlatform.iOS)
					{
						Application.Current.MainPage = new NavigationPage(new RootPage());
					}
					else {
						Application.Current.MainPage = new RootPage();
					}
				}
			};

			var btnEntrarFacebook = new Button {
				Image = AppStyle.Icons.Facebook,
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				Text = "Entra con Facebook",
				TextColor = Color.White,
				BackgroundColor = AppStyle.FacebookBlue,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 302,
				IsVisible = false
				//HeightRequest = 40
			};

			btnEntrarFacebook.Clicked += async (object sender, EventArgs e) => await DisplayAlert ("Información", "Opción de Facebook no Implementada","Ok");

			var btnEntrarGoogle = new Button {
				Image = AppStyle.Icons.GooglePlus,
				FontFamily = AppStyle.FontFamily,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				Text = "Entra con Google+",
				TextColor = Color.White,
				BackgroundColor = AppStyle.GoogleRed, 
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 302,
				IsVisible = false
				//HeightRequest = 40	
			};
			btnEntrarGoogle.Clicked += async (object sender, EventArgs e) => await DisplayAlert ("Información", "Opción de Google+ no Implementada","Ok");

			var skLayout = new StackLayout {
				Padding = new Thickness(20),
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Spacing = 20,
				Children = {txtEmail, txtPassword, lblOlvido }
			};

			var skBottoms = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.Start,
				Children = {btnEntrar, btnEntrarFacebook, btnEntrarGoogle}
					
			};

			var gridLayout = new Grid {
				Padding = new Thickness(5),
				BackgroundColor = Color.Transparent,
				ColumnSpacing = 0,
				RowSpacing = 0,
				RowDefinitions = { new RowDefinition { Height = new GridLength(10, GridUnitType.Star) } ,
								   new RowDefinition { Height = new GridLength(5, GridUnitType.Star) } ,
								   new RowDefinition { Height = new GridLength(50, GridUnitType.Star) } ,
					               new RowDefinition { Height = new GridLength(35,GridUnitType.Star) } 
				                 },
				ColumnDefinitions = {
					new ColumnDefinition { Width = new GridLength(100, GridUnitType.Star) }
				}
			};
			gridLayout.Children.Add (skTop, 0, 0);
			gridLayout.Children.Add (aiLoading, 0, 1);
			gridLayout.Children.Add (skLayout, 0, 2);
			gridLayout.Children.Add (skBottoms, 0, 3);

			var skPrincipal = new StackLayout () { Orientation = StackOrientation.Vertical, Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5)  };

			skPrincipal.Children.Add (gridLayout);
			this.Content = skPrincipal;	
		}    
    }
}

