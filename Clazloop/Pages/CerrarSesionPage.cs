﻿using Xamarin.Forms;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class CerrarSesionPage : ContentPage
	{
		public CerrarSesionPage ()
		{
			App.IsLogin = false;
			Navigation.PushAsync(new Login());
		}
	}
}
