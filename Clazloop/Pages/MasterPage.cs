﻿using System;
using Xamarin.Forms;
using Clazloop.DTOs;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class MasterPage : ContentPage
	{
		public MasterPage ()
		{
			if (Device.OS == TargetPlatform.iOS) {
				this.Padding = new Thickness(5, Device.OnPlatform(10, 0, 0), 5, 5);
			}
			else {
				this.Padding = new Thickness(0, 0, 0, 0);
				ToolbarItems.Add(new ToolbarItem("", "search_icon.png", () =>
					{
						Navigation.PushAsync (new SearchPage());
					}));

			}
			this.BackgroundColor = Color.White;
		}
	}
}

