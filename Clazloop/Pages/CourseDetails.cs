﻿using System;
using Xamarin.Forms;


namespace Clazloop
{
	public class CourseDetails: MasterPage
	{
		public CourseDetails ()
		{ 
			string url = @"<head><link href=""http://vjs.zencdn.net/5.2.2/video-js.css"" rel=""stylesheet""><script src=""http://vjs.zencdn.net/ie8/1.1.0/videojs-ie8.min.js""></script></head><body><video id=""my-video"" class=""video-js"" controls preload=""auto"" width=""400"" height=""200"" poster=""https://clazloop.com/imagesApp/course_30.png"" data-setup=""{}""><source src=""https://youtu.be/xA6-FKL73PQ"" type='video/mp4'><source src=""https://youtu.be/xA6-FKL73PQ"" type='video/webm'><p class=""vjs-no-js"">To view this video please enable JavaScript, and consider upgrading to a web browser that<a href="" http://videojs.com/html5-video-support/"" target=""_blank"">supports HTML5 video</a></p></video><script src=""http://vjs.zencdn.net/5.2.2/video.js""></script></body>";
			var htmlSource = new HtmlWebViewSource();
			htmlSource.Html = url;
			this.Title = "Detalle Curso";
			var webContent = new WebView ();
			webContent.Source = htmlSource;
			webContent.VerticalOptions = LayoutOptions.FillAndExpand;
			webContent.HorizontalOptions = LayoutOptions.FillAndExpand;
			var sk = new StackLayout () {
				Orientation = StackOrientation.Horizontal
			};
			sk.Children.Add (webContent);
			this.Content = sk;

		}
	}
}

