﻿using System;
using Xamarin.Forms;
using Telerik.XamarinForms.Common;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using System.Collections;
using Clazloop.ListViewTemplates;
using Clazloop.DTOs;
using System.Linq;
using System.Collections.ObjectModel;

namespace Clazloop
{
	[Preserve(AllMembers=true)]
	public class DashBoardPage : MasterPage
	{
		RadListView _listHistory;
		RadListView _listNews;
		RadListView _listPlayList;
		RadListView _listSuggestion;

		public DashBoardPage ()
		{
			Title = "DashBoard";
			Icon = "ic_Home";
			var layout = new StackLayout {
				Padding = new Thickness(0, 5, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
					
			var skOuterContinuar = Opcion (_listHistory, App.UserInfo.coursesHistory,"CONTINUAR VIENDO",EnumTipoTemplate.COURSE, EnumElementoDashBoard.HISTORIAL);
			var skOuterCarpeta = Opcion (_listPlayList, App.UserInfo.PlayList,"CARPETAS PLAYLIST",EnumTipoTemplate.PLAYLIST,EnumElementoDashBoard.PLAYLIST);
			var skOuterRecomendados = Opcion (_listSuggestion, App.UserInfo.coursesSuggestion,"CURSOS RECOMENDADOS",EnumTipoTemplate.COURSE,EnumElementoDashBoard.RECOMENDADOS);
			var skOuterNuevos = Opcion (_listNews, App.UserInfo.coursesNew,"NUEVOS CURSOS",EnumTipoTemplate.COURSE,EnumElementoDashBoard.NUEVOS);

			var skFooter = new StackLayout {
				Padding = 50,
				Spacing = 15,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			var imgBanner = new Image {Source = AppStyle.Icons.BannerTop};
			var imgBanner2 = new Image {Source = AppStyle.Icons.BannerBottom};

			var btnBoton = new Button () {
				FontFamily = AppStyle.FontFamily,
				Text = "Biblioteca de Cursos",
				TextColor = Color.White,
				BackgroundColor = Color.Purple,
				HeightRequest = 40,
				WidthRequest = 302
			};

			btnBoton.Clicked += (object sender, EventArgs e) => Navigation.PushAsync (new BibliotecaPage ()); 

			skFooter.Children.Add (imgBanner);
			skFooter.Children.Add (imgBanner2);
			skFooter.Children.Add (btnBoton);
			if (App.UserInfo.coursesHistory.Count != 0) {
				layout.Children.Add (skOuterContinuar);
				layout.Children.Add (ObtlayoutSpacing());
			}
			if (App.UserInfo.PlayList.Count != 0) {
				layout.Children.Add (skOuterCarpeta);
				layout.Children.Add (ObtlayoutSpacing());
			}
			if (App.UserInfo.coursesSuggestion.Count != 0) {
				layout.Children.Add (skOuterRecomendados);
				layout.Children.Add (ObtlayoutSpacing());
			}
			if (App.UserInfo.coursesNew.Count != 0) {
				layout.Children.Add (skOuterNuevos);
				layout.Children.Add (ObtlayoutSpacing());
			}

			layout.Children.Add (skFooter);
			ScrollView scrollViewContent = new ScrollView { Content = layout, Orientation = ScrollOrientation.Vertical };
			Content = scrollViewContent;
		}


		protected StackLayout ObtlayoutSpacing()
		{
			var layoutSpacing = new StackLayout {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
				HeightRequest = 8,
				MinimumHeightRequest = 8,
			};

			return layoutSpacing;
		}

		StackLayout Opcion (RadListView Lista, IList Datos, string Titulo, EnumTipoTemplate Tipo, EnumElementoDashBoard TipoDashBoard)
		{
			var ftGesture1 = new TapGestureRecognizer ();
			ftGesture1.Tapped += (object sender, EventArgs e) => Navigation.PushAsync (new CursosList (Datos,TipoDashBoard));
			var lblContinuar = new Label (){ Text = Titulo, Style = AppStyle.DashBoardTitleStyle }; 
			var lblContinuarMas = new Label (){ Text = "Ver Más >> ", Style = AppStyle.DashBoardSubTitleStyle };  
			lblContinuarMas.GestureRecognizers.Add (ftGesture1);
			var skInnerContinuar = new StackLayout {
				Spacing = 10,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children =  { lblContinuar, lblContinuarMas }
			};

			Lista = new RadListView ();
			Lista.ItemStyle = AppStyle.ListViewLinearLayoutAllBorder;
			Lista.SelectedItemStyle =  AppStyle.ListViewLinearLayoutAllBorder;
			switch (Device.OS) {
			case TargetPlatform.Android:
				Lista.HeightRequest = App.ScreenHeight / 4;
				break;
			case TargetPlatform.iOS:
				Lista.HeightRequest = App.ScreenHeight == 480 ? App.ScreenHeight / 3 : App.ScreenHeight / 3.5;
				break;
			}
			Lista.ItemsSource = Datos;
			Lista.LayoutDefinition = new ListViewLinearLayout { Orientation = Orientation.Horizontal, HorizontalItemSpacing = 10 };
			if (Tipo == EnumTipoTemplate.COURSE) {
				Lista.ItemTemplate = new DataTemplate (typeof(CourseCardView));
			} else {
				Lista.ItemTemplate = new DataTemplate (typeof(PlayListCardView));
			}
			Lista.SelectionChanged +=  (sender, e) => {
				if (((RadListView)sender).SelectedItems.Count == 0) return;
				Lista.IsEnabled = false;
				if (((RadListView)sender).SelectedItems[0] is CourseRestDTO)  CourseSelected(sender);
				if (((RadListView)sender).SelectedItems[0] is PlayListRestDTO) PlayListSelected(sender);
				Lista.IsEnabled = true;
            };
			var sk1 = new StackLayout {
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				Children =  { skInnerContinuar, Lista }
			};
			return sk1;
		}

		private async void CourseSelected(object item)
		{
			if (((RadListView)item).SelectedItems.Count == 0) return;
			var itemSelected = (CourseRestDTO)((RadListView)item).SelectedItems[0];
			this.IsBusy = true;
			CourseViewModel curso = new CourseViewModel();
			CourseDetailDTO courseDetail = await curso.GetCourseInfo(App.UserInfo.Email, App.UserInfo.Token, itemSelected.Id);
			if (courseDetail == null)
			{
				await DisplayAlert("Esto es embarazoso", "Ups, ocurrio un error. Intenta más tarde", "Ok");
				this.IsBusy = false;
				return;
			};
			if (Device.OS == TargetPlatform.Android)
			{
				await Navigation.PushAsync(new CourseDetail(courseDetail));
			}
			else {
				await Navigation.PushAsync(new CourseDetailOS(courseDetail));
			}
			this.IsBusy = false;
		}

		private async void PlayListSelected(object item)
		{
			if (((RadListView)item).SelectedItems.Count == 0) return;
			var itemSelected = (PlayListRestDTO)((RadListView)item).SelectedItems[0];
			this.IsBusy = true;
			CourseViewModel curso = new CourseViewModel();
			ObservableCollection<CourseRestDTO> courseDetail = await curso.SearchCoursesPlayList(App.UserInfo.Email, App.UserInfo.Token, itemSelected.Id);
			if (courseDetail == null)
			{
				await DisplayAlert("Esto es embarazoso", "Ups, ocurrio un error. Intenta más tarde", "Ok");
				this.IsBusy = false;
				return;
			};
			await Navigation.PushAsync(new CursosList(courseDetail, EnumElementoDashBoard.PLAYLIST, itemSelected.Name));
			this.IsBusy = false;
		}
		protected override void OnAppearing ()
		{
			base.OnAppearing ();
		}
	}
}
