﻿using System;
using System.Collections.ObjectModel;

namespace Clazloop
{
	public class CourseListData: ObservableCollection<CourseDTO>
	{
		public CourseListData ()
		{
			this.Add(new CourseDTO(10125123,"CRUD with ASP.NET MVC, Web API, EF and Kendo UI",2599394,"Foto_1.jpg",18224512,7373898,7721613,"20150820","Intermedio",1724606976,"20150820","20150820",1602606,6));
			this.Add(new CourseDTO(10775647,"Redistribute OEMKEY",3342037,"Foto_2.jpg",20973777,19163300,14649310,"20150820","Avanzado",1410999346,"20150820","20150820",13994052,6));
			this.Add(new CourseDTO(10799392,"Redistribute OEM",17890271,"Foto_3.jpg",10940863,12770468,10594879,"20150820","Avanzado",330715305,"20150820","20150820",10108150,6));
			this.Add(new CourseDTO(10964209,"ILMerge",7632635,"Foto_4.jpg",5886842,20058659,8827941,"20150820","Basico",2050944242,"20150820","20150820",7027299,6));
			this.Add(new CourseDTO(11262087,"NativeScript 1.2 Release Webinar",1867818,"Foto_5.jpg",7560756,15682903,1600697,"20150820","Intermedio",566945582,"20150820","20150820",10473591,6));
			this.Add(new CourseDTO(11599537,"How To Connect Your Mobile App To Data: Webinar Clip",16365756,"Foto_6.jpg",19612148,6575825,8261077,"20150820","Basico",493049532,"20150820","20150820",17045879,6));
			this.Add(new CourseDTO(11738917,"How To Build A Mobile App With Telerik Platform: Webinar Clip",1106964,"Foto_7.jpg",1536520,12713874,7278663,"20150820","Basico",639773053,"20150820","20150820",4880600,6));
			this.Add(new CourseDTO(11808170,"Choosing The Right Mobile App Development Approach: Webinar Clip",16478156,"Foto_8.jpg",3380937,9700044,5076860,"20150820","Avanzado",1953942870,"20150820","20150820",997753,6));
			this.Add(new CourseDTO(11879880,"Transform Your Mobile App from Idea to Reality - Webinar Intro",7226922,"Foto_9.jpg",12727135,4916941,12316040,"20150820","Avanzado",445280831,"20150820","20150820",14696129,6));
			this.Add(new CourseDTO(12023124,"Kendo UI Building Blocks by Cody Lindley",20739752,"Foto_10.jpg",10022246,7066924,2254992,"20150820","Basico",1147649402,"20150820","20150820",15205067,6));
			this.Add(new CourseDTO(12023815,"How Boston Heart Diagnostics Uses Telerik Kendo UI",5439873,"Foto_11.jpg",13275506,20889926,5610835,"20150820","Intermedio",1614079505,"20150820","20150820",13330469,6));
			this.Add(new CourseDTO(12024419,"Summer of NativeScript, Louisville",13318797,"Foto_12.jpg",11354059,10951118,3014775,"20150820","Basico",203626518,"20150820","20150820",10101834,6));
			this.Add(new CourseDTO(12026145,"What's new in Sitefinity 8.1",13405974,"Foto_13.jpg",10505600,6424466,962221,"20150820","Avanzado",499504720,"20150820","20150820",18809839,6));
			this.Add(new CourseDTO(12370592,"S1E4 Running a successful open source project",16313199,"Foto_14.jpg",4130583,1853130,19944583,"20150820","Avanzado",210344258,"20150820","20150820",9568284,6));
			this.Add(new CourseDTO(12450324,"Kendo UI Webinar: Build Responsive or Bust",11025546,"Foto_15.jpg",20321269,11331853,1207328,"20150820","Basico",338220259,"20150820","20150820",12330506,6));
			this.Add(new CourseDTO(12488809,"Eat Sleep Code – Season 1, Episode 3",12931097,"Foto_16.jpg",567817,500623,13229248,"20150820","Intermedio",749330001,"20150820","20150820",9221069,6));
			this.Add(new CourseDTO(12700991,"Eat Sleep Code – Season 1, Episode 2",617061,"Foto_17.jpg",9031645,20350318,15905316,"20150820","Intermedio",1856512331,"20150820","20150820",6295761,6));
			this.Add(new CourseDTO(12726487,"The New .NET is Coming Fast: 5 Tips to Get You Ready",16163449,"Foto_18.jpg",1859572,15755905,9436274,"20150820","Basico",499352527,"20150820","20150820",2665730,6));
			this.Add(new CourseDTO(13084307,"DevCraft June 2015 Release Webinar",13729656,"Foto_19.jpg",14618718,5106179,6315369,"20150820","Intermedio",129894609,"20150820","20150820",7065824,6));
			this.Add(new CourseDTO(13268914,"Debugging with Fiddler by Eric Lawrence",20121826,"Foto_20.jpg",4450894,3271824,16475941,"20150820","Avanzado",1108233304,"20150820","20150820",14616032,6));

			this.Add(new CourseDTO(13842704,"Capitulo 1: Administración",15328027,"Foto_21.jpg",3077080,15373811,5082720,"20150820","Avanzado",527892702,"20150820","20150820",2850738,1));
			this.Add(new CourseDTO(13884755,"Administración conceptos básicos - CURSOS FACILITOS",10196819,"Foto_22.jpg",19007341,18351141,14762339,"20150820","Intermedio",1642261576,"20150820","20150820",17692739,1));
			this.Add(new CourseDTO(13990002,"Tutorial de Administración de Empresas Historia de la Administración",18142851,"Foto_23.jpg",21012908,20597702,9797082,"20150820","Intermedio",101964369,"20150820","20150820",16034293,1));
			this.Add(new CourseDTO(13991265,"Concepto de Administración - ADMINISTRACION PARA TODOS",11612138,"Foto_24.jpg",18734024,2407310,18462018,"20150820","Basico",948167572,"20150820","20150820",664907,1));
			this.Add(new CourseDTO(14030236,"La importancia de una buena administración",16569222,"Foto_25.jpg",1183320,4130822,8624086,"20150820","Avanzado",1712363972,"20150820","20150820",149120,1));
			this.Add(new CourseDTO(14030787,"Administracion Estrategica Que es Estrategia Empresarial",353070,"Foto_26.jpg",5637469,19040254,18818660,"20150820","Basico",684770960,"20150820","20150820",14280196,1));
			this.Add(new CourseDTO(14093684,"10 cosas por las que estudiar Administración de Empresas",13860326,"Foto_27.jpg",18624613,3899179,5193449,"20150820","Intermedio",1361351657,"20150820","20150820",19745995,1));
			this.Add(new CourseDTO(14175153,"Etapas de la administración (ejemplo)",6249272,"Foto_28.jpg",2984181,2394306,9202845,"20150820","Basico",241886393,"20150820","20150820",12476084,1));
			this.Add(new CourseDTO(14271112,"Administración y sus características - Administración - Educatina",11699295,"Foto_29.jpg",2653648,9907757,20671354,"20150820","Basico",1050603816,"20150820","20150820",11997626,1));
			this.Add(new CourseDTO(14334685,"Administración de Empresas: resuelve tus dudas sobre esta carrera",18943707,"Foto_30.jpg",7980535,14364419,15090203,"20150820","Intermedio",1672386860,"20150820","20150820",18780929,1));

			this.Add(new CourseDTO(14404615,"Artes - Taller de Dibujo. Facultad de Arte y Diseño",10172861,"Foto_31.jpg",15533184,1247456,12186939,"20150820","Avanzado",1933082558,"20150820","20150820",4066575,2));
			this.Add(new CourseDTO(14405988,"Foro de la Cultura 2014 - Mesa de debate - CULTURA, ARTE Y DISEÑO",5215735,"Foto_32.jpg",12836513,493635,20628800,"20150820","Avanzado",1502227753,"20150820","20150820",7582691,2));
			this.Add(new CourseDTO(14760642,"El arte de crear diseño de cartel",17010442,"Foto_33.jpg",1029239,13442354,20678997,"20150820","Intermedio",1131203778,"20150820","20150820",8077364,2));
			this.Add(new CourseDTO(14877061,"Documental Arte y Diseño protesta",12638648,"Foto_34.jpg",8091451,11395908,1467896,"20150820","Intermedio",2102030462,"20150820","20150820",4738517,2));
			this.Add(new CourseDTO(15150540,"Escuela Profesional de Arte & Diseño",18815645,"Foto_35.jpg",12602432,6686961,13116273,"20150820","Avanzado",1455128283,"20150820","20150820",10388651,2));
			this.Add(new CourseDTO(15264652,"Carrera de Arte y Diseño Empresarial",19995369,"Foto_36.jpg",6311774,17551612,20145395,"20150820","Basico",332215686,"20150820","20150820",12292896,2));
			this.Add(new CourseDTO(15307809,"Como revelar un arte o diseño - Serigrafia",19542297,"Foto_37.jpg",1669693,12684507,7496396,"20150820","Basico",1966876504,"20150820","20150820",868179,2));
			this.Add(new CourseDTO(15444238,"Diseño Gráfico en IPAD: Convierte tu talento en una profesión",19515163,"Foto_38.jpg",7373629,14888524,11786909,"20150820","Intermedio",589622555,"20150820","20150820",216286,2));
			this.Add(new CourseDTO(15447857,"Escuela de Arquitectura, Arte y Diseño",4865771,"Foto_39.jpg",13768482,7266563,4122559,"20150820","Basico",2089260212,"20150820","20150820",2815188,2));
			this.Add(new CourseDTO(15540486,"Diseño, Animación y Arte Digital",3483081,"Foto_40.jpg",20913128,1872289,11753539,"20150820","Intermedio",2037352406,"20150820","20150820",13197515,2));

			this.Add(new CourseDTO(16088125,"El Sistema Solar",15720896,"Foto_41.jpg",17794561,16742873,1802333,"20150820","Basico",1274687769,"20150820","20150820",20030869,3));
			this.Add(new CourseDTO(16299209,"Los vertebrados",17997918,"Foto_42.jpg",16369249,5258963,19728514,"20150820","Basico",2103001878,"20150820","20150820",21077734,3));
			this.Add(new CourseDTO(16299210,"Magnitudes: las unidades de longitud",9202898,"Foto_43.jpg",20324635,7623245,18997833,"20150820","Intermedio",478378460,"20150820","20150820",21171057,3));
			this.Add(new CourseDTO(16385367,"El Universo",3263175,"Foto_44.jpg",7195721,17989864,17763956,"20150820","Basico",981636922,"20150820","20150820",9081329,3));
			this.Add(new CourseDTO(16442821,"El esqueleto",7759074,"Foto_45.jpg",707702,5712254,12253194,"20150820","Intermedio",2027583111,"20150820","20150820",2635065,3));
			this.Add(new CourseDTO(16899887,"La multiplicación. Introducción",5726399,"Foto_46.jpg",19477811,9718527,6980180,"20150820","Basico",2034775878,"20150820","20150820",3908272,3));
			this.Add(new CourseDTO(17034655,"Los eclipses",15934611,"Foto_47.jpg",226926,4617231,6075808,"20150820","Basico",1946212373,"20150820","20150820",10784538,3));
			this.Add(new CourseDTO(17229776,"El adjetivo",12650780,"Foto_48.jpg",5076609,12933457,11195255,"20150820","Avanzado",2146008891,"20150820","20150820",18120021,3));
			this.Add(new CourseDTO(17573510,"La función de nutrición",5469519,"Foto_49.jpg",8296301,3055048,1159555,"20150820","Basico",537229998,"20150820","20150820",16406605,3));
			this.Add(new CourseDTO(17732340,"La división. Introducción",6326771,"Foto_50.jpg",15604386,16373661,9418128,"20150820","Basico",973556964,"20150820","20150820",15081182,3));
		}
	}
}

