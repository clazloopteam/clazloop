﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Clazloop
{
	public class CategoryListData: ObservableCollection<CategoryDTO>
	{
		public CategoryListData ()
		{
			this.Add (new CategoryDTO (1,"Administración","Lorem Ipsum", "administracion.png"));
			this.Add (new CategoryDTO (2,"Artes y Diseño","Lorem Ipsum", "arte.png"));
			this.Add (new CategoryDTO (3,"Ciencias","Lorem Ipsum", "ciencia.png"));
			this.Add (new CategoryDTO (4,"Humanidades", "Lorem Ipsum","humanidades.png"));
			this.Add (new CategoryDTO (5,"Ingeniería","Lorem Ipsum", "ingenieria.png"));
			this.Add (new CategoryDTO (6,"Salud","Lorem Ipsum", "salud.png"));
			this.Add (new CategoryDTO (7,"Tecnología","Lorem Ipsum", "tecnologia.png"));
		}
	}

	public class SubCategoryListData: ObservableCollection<CategoryDTO>
	{
		public SubCategoryListData()
		{
			//NOTE: Subcategoria de Administracion.
			this.Add (new CategoryDTO (1,"Inteligencia Financiera","Lorem Ipsum", "",1));
			this.Add (new CategoryDTO (2,"Inteligencia Empresarial","Lorem Ipsum", "",1));
			this.Add (new CategoryDTO (3,"Productividad","Lorem Ipsum", "",1));
			this.Add (new CategoryDTO (4,"Administraciòn Empresarial","Lorem Ipsum", "",1));
			this.Add (new CategoryDTO (5,"Comunicacion","Lorem Ipsum", "",1));
			this.Add (new CategoryDTO (6,"Liderazgo","Lorem Ipsum", "",1));
			this.Add (new CategoryDTO (7,"Administraciòn de Proyectos","Lorem Ipsum", "",1));

			//NOTE: SubCategoria de Tecnología
			this.Add (new CategoryDTO (8,"Programacion","Lorem Ipsum", "",7));
			this.Add (new CategoryDTO (9,"Arquitectura", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (10,"Diseño Web", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (11,"Lenguajes de Programacion", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (12,"Aplicaciones Moviles", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (13,"Servidores", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (14,"Base de Datos", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (15,"Proyectos", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (16,"Aplicaciones de Escritorio", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (17,"Juegos", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (18,"Computacion en la Nube", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (19,"Web Movil", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (20,"Fundamentos de Programacion", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (21,"Herramientas de Desarrollo", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (22,"Diseño de Juegos", "Lorem Ipsum","",7));
			this.Add (new CategoryDTO (23,"Diseño de Patrones", "Lorem Ipsum","",7));

			//NOTE: Subcategoria de Humanidades
			this.Add (new CategoryDTO (25,"Herramientas de Enseñanza", "Lorem Ipsum","",4));
			this.Add (new CategoryDTO (26,"Elearning", "Lorem Ipsum","",4));
			this.Add (new CategoryDTO (27,"Tecnologia Educacional", "Lorem Ipsum","",4));
			this.Add (new CategoryDTO (28,"Desarrollo Profesional", "Lorem Ipsum","",4));
			this.Add (new CategoryDTO (29,"Diseño Instruccional", "Lorem Ipsum","",4));
			this.Add (new CategoryDTO (30,"Administracion de Clases", "Lorem Ipsum","",4));

			//NOTE: Subcategoria de Arte y Diseño
			this.Add (new CategoryDTO (31,"Color", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (32,"Chispa Creativa", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (33,"Diseño Empresarial", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (34,"Fundamentos de Diseño", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (35,"Conocimiento de Diseño", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (36,"Tecnicas de Diseño", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (37,"Pintura Digital", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (38,"Publicidad Digital", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (39,"Dibujo", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (40,"Ilustracion", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (41,"Infografia", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (42,"Diseño de Logos", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (43,"Infografia", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (44,"Diseño de Pagina", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (45,"Diseño de Impresion", "Lorem Ipsum","",2));
			this.Add (new CategoryDTO (46,"Tipografia", "Lorem Ipsum","",2));

			//NOTE:Subcategoria de Ciencias
			this.Add (new CategoryDTO (47,"Cuerpo Humano", "Lorem Ipsum","",3));
			this.Add (new CategoryDTO (48,"Fisica", "Lorem Ipsum","",3));
			this.Add (new CategoryDTO (49,"Quimica", "Lorem Ipsum","",3));
			this.Add (new CategoryDTO (50,"Medicina", "Lorem Ipsum","",3));
			this.Add (new CategoryDTO (51,"Nutricion", "Lorem Ipsum","",3));
		}
	}
}

