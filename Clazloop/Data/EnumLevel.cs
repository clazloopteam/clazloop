﻿using System;

namespace Clazloop
{
	public enum EnumLevel
	{
		TODOS = 0,
		PUBLICO,
		BASICO,
		INTERMEDIO,
		ADVANZADO
	}

	public enum EnumTipoTemplate
	{
		COURSE = 0,
		PLAYLIST
	}

	public enum EnumElementoDashBoard
	{
		HISTORIAL = 0,
		PLAYLIST,
		RECOMENDADOS,
		NUEVOS,
		FAVORITOS
	}

	public enum EnumTipoFiltro
	{
		TEMATICA = 0,
		INSTRUCTOR,
		NIVEL
	}
}
