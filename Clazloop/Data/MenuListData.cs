﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Clazloop
{
	public class MenuListData : List<MenuItem>
	{
		public MenuListData ()
		{

			this.Add (new MenuItem () { 
				Title = " Video Cursos", 
				IconSource = "ic_home.png",
				TargetType = typeof(DashBoardPage)
			});

			this.Add (new MenuItem () { 
				Title = "Categorias", 
				IconSource = "ic_biblioteca.png", 
				TargetType = typeof(BibliotecaPage)
			});

			this.Add (new MenuItem () {
				Title = " Clazbooks",
				IconSource = "ic_cursos.png",
				TargetType = typeof(CategoriaPage)
			});

			this.Add (new MenuItem () {
				Title = " Historial",
				IconSource = "ic_cursos.png",
				TargetType = typeof(CursosList)
			});

			this.Add (new MenuItem () {
				Title = " Favoritos",
				IconSource = "ic_cursos.png",
				TargetType = typeof(CursosList)
			});
            

			this.Add (new MenuItem () {
				Title = " Ajustes",
				IconSource = "ic_ajustes.png",
				TargetType = typeof(AjustesPage)
			});

			this.Add (new MenuItem () {
				Title = " Contactanos",
				IconSource = "ic_contactanos.png",
				TargetType = typeof(ContactoPage)
			});

			this.Add (new MenuItem () {
				Title = " Cerrar Sesión",
				IconSource = "ic_cerrar.png",
				TargetType = typeof(CerrarSesionPage)
			});

		}
	}
}

