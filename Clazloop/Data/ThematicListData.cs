﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Clazloop.DTOs;
              
namespace Clazloop
{
	public enum EnumTematica
	{
		ADMINISTRACION =1,
		ARTEYDISENO,
		HUMANIDADES,
		SALUD,
		INGENIERIA,
		TECNOLOGIA,
		CIENCIA
	}

	public class ThematicListData: ObservableCollection<ThematicDTO>
	{
		public ThematicListData ()
		{
			this.Add (new ThematicDTO (1,"Administración","administracion.png"));
			this.Add (new ThematicDTO (2,"Artes y Diseño", "arte.png"));
			this.Add (new ThematicDTO (3,"Humanidades","humanidades.png"));
			this.Add (new ThematicDTO (4,"Salud","salud.png"));
			this.Add (new ThematicDTO (5,"Ingeniería","ingenieria.png"));
			this.Add (new ThematicDTO (6,"Tecnología","tecnologia.png"));
			this.Add (new ThematicDTO (7,"Ciencias","ciencia.png"));
		}
	}
}

