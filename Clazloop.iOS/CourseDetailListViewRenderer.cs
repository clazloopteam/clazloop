﻿using System;
using Clazloop.ListViewTemplates;
using Clazloop.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;
using Clazloop.Controls.CourseDetail;

[assembly: ExportRenderer (typeof(CourseDetailListView), typeof(CourseDetailListViewRenderer))]
namespace Clazloop.iOS
{
	public class CourseDetailListViewRenderer: ListViewRenderer
	{
		public CourseDetailListViewRenderer ()
		{
			
		}

		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);
			if (this.Control == null) return;
			this.Control.TableFooterView = new UIView();
		}
	}
}

