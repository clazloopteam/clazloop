﻿using System;
using Foundation;
using Clazloop.DTOs;
using Telerik.XamarinForms.DataControlsRenderer.iOS;

namespace Clazloop.iOS
{
	public class NSItemCourseV2 : ConvertibleNSObject<CourseRestDTOV2>
	{
		public NSItemCourseV2(CourseRestDTOV2 instance) : base(instance)
		{
		}

		[Export("AuthorFullName")]
		public string AuthorFullName
		{
			get
			{
				return this.Instance.AuthorFullName;
			}
		}

		[Export("FullUrlImage")]
		public string FullUrlImage
		{
			get
			{
				return this.Instance.FullUrlImage;
			}
		}

		[Export("Name")]
		public string Name
		{
			get
			{
				return this.Instance.Name;
			}
		}

		[Export("TotalTime")]
		public string TotalTime
		{
			get
			{
				return this.Instance.TotalTime;
			}
			set
			{
				this.Instance.TotalTime = value;
			}
		}

		[Export("LevelName")]
		public string LevelName
		{
			get {
				return this.Instance.LevelName;
			}
		}

	}
}