﻿using System;
using Foundation;
using Clazloop.DTOs;
using Telerik.XamarinForms.DataControlsRenderer.iOS;

namespace Clazloop.iOS
{
	public class NSItemMenu : ConvertibleNSObject<MenuItem>
	{
		public NSItemMenu(MenuItem instance) : base(instance)
		{
		}

		[Export("Title")]
		public string Title
		{
			get
			{
				return this.Instance.Title;
			}
		}

		[Export("IconSource")]
		public string IconSource
		{
			get
			{
				return this.Instance.IconSource;
			}
		}
	}
}