﻿using System;
using Foundation;
using Clazloop.DTOs;
using Telerik.XamarinForms.DataControlsRenderer.iOS;

namespace Clazloop.iOS
{
	public class NSItemCourse : ConvertibleNSObject<CourseRestDTO>
	{
		public NSItemCourse(CourseRestDTO instance) : base(instance)
		{
		}

		[Export("AuthorFullName")]
		public string AuthorFullName
		{
			get
			{
				return this.Instance.AuthorFullName;
			}
		}

		[Export("FullUrlImage")]
		public string FullUrlImage
		{
			get
			{
				return this.Instance.FullUrlImage;
			}
		}

		[Export("Named")]
		public string Named
		{
			get
			{
				return this.Instance.Named;
			}
		}

		[Export("TotalTime")]
		public string TotalTime
		{
			get
			{
				return this.Instance.TotalTime;
			}
			set
			{
				this.Instance.TotalTime = value;
			}
		}
	}
}