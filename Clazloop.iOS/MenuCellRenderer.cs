﻿using System;
using Clazloop;
using Clazloop.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;

[assembly: ExportRenderer(typeof(MenuCell), typeof(MenuCellRenderer))]

namespace Clazloop.iOS
{
	[Preserve(AllMembers=true)]
	class MenuCellRenderer : ViewCellRenderer
	{
		public MenuCellRenderer()
		{

		}

		public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;           
		}
	}
}


