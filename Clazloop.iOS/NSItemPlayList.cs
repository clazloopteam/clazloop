﻿using System;
using Foundation;
using Clazloop.DTOs;
using Telerik.XamarinForms.DataControlsRenderer.iOS;

namespace Clazloop.iOS
{
	public class NSItemPlayList : ConvertibleNSObject<PlayListRestDTO>
	{
		public NSItemPlayList(PlayListRestDTO instance) : base(instance)
		{
		}

		[Export("Name")]
		public string Name
		{
			get
			{
				return this.Instance.Name;
			}
		}

		[Export("NumberCourses")]
		public Int16 NumberCourses
		{
			get
			{
				return this.Instance.NumberCourses;
			}
		}

		[Export("GetCourseNumber")]
		public string GetCourseNumber
		{
			get
			{
				return this.Instance.GetCourseNumber;
			}
		}
	}
}