﻿using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;
using Clazloop;
using Clazloop.iOS;
using MediaPlayer;
using System.Linq;

[assembly: ExportRenderer(typeof(VideoViewOS), typeof(VideoViewOSRenderer))]

namespace Clazloop.iOS
{
	public class VideoViewOSRenderer : ViewRenderer<VideoViewOS, UIView>
	{
		MPMoviePlayerController _player;
        private List<NSObject> _observers = new List<NSObject> ();

		protected override void OnElementChanged (ElementChangedEventArgs<VideoViewOS> e)
		{
			base.OnElementChanged (e);
			if (e.NewElement != null)
			{
				if (base.Control == null)
				{
                    _player = new MPMoviePlayerController();
					_player.ScalingMode = MPMovieScalingMode.Fill;
					_player.InitialPlaybackTime = 2.0;
					_player.ShouldAutoplay = false;
                    _player.BackgroundColor = UIColor.Clear;
                    base.SetNativeControl(_player.View);
                    var center = NSNotificationCenter.DefaultCenter;
					_observers.Add(center.AddObserver((NSString)"UIDeviceOrientationDidChangeNotification", DeviceRotated));
					_observers.Add(center.AddObserver(MPMoviePlayerController.PlaybackStateDidChangeNotification, OnLoadStateChanged));
					_observers.Add(center.AddObserver(MPMoviePlayerController.PlaybackDidFinishNotification, OnPlaybackComplete));
					_observers.Add(center.AddObserver(MPMoviePlayerController.WillExitFullscreenNotification, OnExitFullscreen));
					_observers.Add(center.AddObserver(MPMoviePlayerController.WillEnterFullscreenNotification, OnEnterFullscreen));
                    ToggleFullscreen ();
				}
			}
			updateVideoPath();
			updateFullscreen();
		}

        private void DeviceRotated(NSNotification notification)
		{
			ToggleFullscreen ();
		}

		private void OnLoadStateChanged(NSNotification notification)
		{

			if (_player.LoadState == MPMovieLoadState.Unknown)
			{
				_player.ContentUrl = NSUrl.FromString(this.Element.VideoSource);
				_player.PrepareToPlay();
			}
		}
		private void OnPlaybackComplete(NSNotification notification) { }

		private void OnExitFullscreen(NSNotification notification) { }  

		private void OnEnterFullscreen (NSNotification notification) { }

		private void ToggleFullscreen()
		{
			_player.ScalingMode = MPMovieScalingMode.None;
            switch (UIDevice.CurrentDevice.Orientation)
			{
			case UIDeviceOrientation.Portrait:
				_player.View.Frame = new CoreGraphics.CGRect(0, 0, (double)UIScreen.MainScreen.Bounds.Width, (double)UIScreen.MainScreen.Bounds.Height / 3.2);
				_player?.SetFullscreen(false, false);
				break;
			//case UIDeviceOrientation.PortraitUpsideDown:
			//	_player.View.Frame = new CoreGraphics.CGRect(0, 0, (double)UIScreen.MainScreen.Bounds.Height / 3.2,  (double)UIScreen.MainScreen.Bounds.Width);	
			//	_player?.SetFullscreen (false, false);
			//	break;
			case UIDeviceOrientation.LandscapeLeft:
				_player.View.Frame = new CoreGraphics.CGRect(0, 0, (double)UIScreen.MainScreen.Bounds.Width, (double)UIScreen.MainScreen.Bounds.Height);	
				_player?.SetFullscreen(true, false);
				break;
			case UIDeviceOrientation.LandscapeRight:
				_player.View.Frame = new CoreGraphics.CGRect(0, 0, (double)UIScreen.MainScreen.Bounds.Width, (double)UIScreen.MainScreen.Bounds.Height);	
				_player?.SetFullscreen(true, false);
				break;
			}
			_player.View.SetNeedsLayout();
			_player.View.SetNeedsDisplay();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == VideoViewOS.VideoSourceProperty.PropertyName)	updateVideoPath();
			if (e.PropertyName == VideoViewOS.FullscreenProperty.PropertyName) updateFullscreen();
		}

		public override void MovedToSuperview() 
		{
			base.MovedToSuperview();
		}

		private void updateVideoPath()
		{
			if (_player == null) return;
            _player.ControlStyle = MPMovieControlStyle.Embedded;
            _player.ShouldAutoplay = true;
			_player.SourceType = MPMovieSourceType.Streaming;
			_player.BackgroundView.BackgroundColor = UIColor.White;
			_player.BackgroundColor = UIColor.White;
			_player.ContentUrl = !string.IsNullOrWhiteSpace(this.Element.VideoSource) ? NSUrl.FromString(this.Element.VideoSource) : null;
			_player.PrepareToPlay();
		}

		private void updateFullscreen()
		{
			if (_player == null) return;
			_player.SetFullscreen(this.Element.Fullscreen, true);
			_player.View.SetNeedsLayout();
			_player.View.SetNeedsDisplay();
		}

        protected override void Dispose(bool disposing)
		{
			if (this._player != null)
			{
				this._player.Stop();
				this._player.Dispose();
				this._player = null;
				NSNotificationCenter.DefaultCenter.RemoveObservers(_observers);
			}
			base.Dispose(disposing);
		}
	}
}