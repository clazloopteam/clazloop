﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Foundation;
using Clazloop.DTOs;
using Telerik.XamarinForms.DataControlsRenderer.iOS;

namespace Clazloop.iOS
{
	public class NSItemThematic : ConvertibleNSObject<ThematicDTO>
	{
		public NSItemThematic(ThematicDTO instance) : base(instance)
		{
		}

		[Export("Id")]
		public Int16 Id
		{
			get
			{
				return this.Instance.Id;
			}
		}

		[Export("Name")]
		public string Name
		{
			get
			{
				return this.Instance.Name;
			}
		}

		[Export("Image")]
		public string Image
		{
			get
			{
				return this.Instance.Image;
			}
		}
	}


//	public class NSItemFilter : ConvertibleNSObject<FilterCourseDTO>
//	{
//		public NSItemFilter(FilterCourseDTO instance) : base(instance)
//		{
//		}
//
//		[Export("CategoriasHijo")]
//		public ObservableCollection <ThematicDTO> CategoriasHijo
//		{
//			get
//			{
//				return this.Instance.CategoriasHijo;
//			}
//		}
//
//		[Export("Cursos")]
//		public ObservableCollection <CourseRestDTOV2> Cursos
//		{
//			get
//			{
//				return this.Instance.Cursos;
//			}
//		}
//
//		[Export("LevelDTO")]
//		public ObservableCollection <LevelDTO> Niveles
//		{
//			get
//			{
//				return this.Instance.Niveles;
//			}
//		}
//
//		[Export("AuthorRestDTO")]
//		public ObservableCollection <AuthorRestDTO> Instructores
//		{
//			get { return this.Instance.Instructores; }
//		}
//
//	}

}