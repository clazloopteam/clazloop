﻿using System;
using Foundation;
using Clazloop.DTOs;
using Telerik.XamarinForms.DataControlsRenderer.iOS;
using UIKit;
using ImageCircle.Forms.Plugin.iOS;
using DeviceOrientation.Forms.Plugin.iOS;
using FormsNativeVideoPlayer.iOS;

namespace Clazloop.iOS
{
	[Register ("AppDelegate")]

	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{            
			new Telerik.XamarinForms.DataControlsRenderer.iOS.ListViewRenderer ();

			global::Xamarin.Forms.Forms.Init ();
			DeviceOrientationImplementation.Init();
			ImageCircleRenderer.Init();
			IOSTypeConverter.Register<CourseRestDTO, NSItemCourse>();
			IOSTypeConverter.Register<ThematicDTO, NSItemThematic>();
			IOSTypeConverter.Register<PlayListRestDTO, NSItemPlayList>();
			IOSTypeConverter.Register<CourseRestDTOV2, NSItemCourseV2>();
			IOSTypeConverter.Register<MenuItem, NSItemMenu>();
			System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };
			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
			#endif
			//Set static double values in our app to size elements off of screen size
			//Important to note that iOS Forms utilizes real pixels to size elements 
			//This is why we can just grab the pixels of the screen
			App.ScreenWidth = (double)UIScreen.MainScreen.Bounds.Width;
			App.ScreenHeight = (double)UIScreen.MainScreen.Bounds.Height;

			//Set our iOS Device so we can utilize it in our CustomRenderer for screen orientation
			DeviceHelper.iOSDevice = UIDevice.CurrentDevice;
			UINavigationBar.Appearance.BackgroundColor = UIColor.FromRGB(21,111,167);
			UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(21,111,167);
			UINavigationBar.Appearance.TintColor = UIColor.White; 
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
				{
					Font = UIFont.FromName("HelveticaNeue-CondensedBold", (nfloat)16f),
					TextColor = UIColor.White
				});
		    LoadApplication (new App ());
			return base.FinishedLaunching (app, options);
		}

		//[Export ("application:supportedInterfaceOrientationsForWindow:")]
		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations (UIApplication application, UIWindow forWindow)
		{
			if (forWindow != null) {
				if (forWindow.RootViewController.PresentedViewController is MediaPlayer.MPMoviePlayerViewController &&
					!forWindow.RootViewController.PresentedViewController.IsBeingDismissed)
					return UIInterfaceOrientationMask.Portrait;
			}

			return UIInterfaceOrientationMask.All;
		}
	}


}




