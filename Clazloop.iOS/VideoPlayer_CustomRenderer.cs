﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using FormsNativeVideoPlayer.iOS;
using AVFoundation;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreMedia;
using AVKit;
using Clazloop;
using MediaPlayer;

[assembly: ExportRenderer(typeof(VideoViewOS), typeof(VideoPlayer_CustomRenderer))]

namespace FormsNativeVideoPlayer.iOS
{
	public class VideoPlayer_CustomRenderer : ViewRenderer
	{
		AVPlayer _player;
		AVPlayerLayer _playerLayer;
		string videoPath;
		VideoViewOS ws;

		protected override void OnElementChanged (ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);
			if (e.NewElement == null) return;

			ws = (VideoViewOS)e.NewElement;
			videoPath = ws.VideoSource;
//			Rectangle r =  e.NewElement.Bounds;
//			CGRect rect = CGRect.FromLTRB ((nfloat)r.Left, (nfloat)r.Top, (nfloat)r.Right, (nfloat)r.Bottom);
//			UIView temp = new UIView(rect);
//			SetNativeControl (temp);
			StartVideo();
		}

		void StartVideo ()
		{
			_player = new AVPlayer (new NSUrl (videoPath));
			_playerLayer = AVPlayerLayer.FromPlayer (_player);
			_playerLayer.Frame = NativeView.Bounds;
			NativeView.Layer.AddSublayer (_playerLayer);
			_player.Play ();
		}
			
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			_playerLayer = AVPlayerLayer.FromPlayer (_player);
			_playerLayer.Frame = NativeView.Bounds;
			NativeView.Layer.AddSublayer (_playerLayer);
		}
	}
}