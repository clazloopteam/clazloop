﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using Clazloop;
using Clazloop.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.Net;
using System.IO;
using Android.Graphics;
using Android.Util;
using Android.OS;



[assembly: ExportRenderer (typeof(CourseViewCellLineal_Two), typeof(NativeAndroidCellRenderer))]
namespace Clazloop.Droid
{
	public class NativeAndroidCellRenderer : ViewCellRenderer
	{
		protected override Android.Views.View GetCellCore (Xamarin.Forms.Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
		{
			var x = (CourseViewCellLineal_Two)item;
			var view = convertView;
			if (view == null) view = (context as Activity).LayoutInflater.Inflate (Resource.Layout.NativeAndroidCell, null);	
			view.FindViewById<TextView> (Resource.Id.Named).Text = x.Named;
			view.FindViewById<TextView> (Resource.Id.AuthorFullName).Text = x.AuthorFullName;
			view.FindViewById<TextView> (Resource.Id.TotalTime).Text = x.TotalTime;
			view.FindViewById<TextView> (Resource.Id.LevelName).Text = x.LevelName;

			var imgUrl = Android.Net.Uri.Parse(x.FullUrlImage);
			var imageBitmap = GetImageBitmapFromUrl(x.FullUrlImage);
			int currentBitmapWidth = imageBitmap.Width;
			int currentBitmapHeight = imageBitmap.Height;
			int ivWidth = view.FindViewById<ImageView> (Resource.Id.FullUrlImage).LayoutParameters.Width;
			int ivHeight = view.FindViewById<ImageView> (Resource.Id.FullUrlImage).LayoutParameters.Height;
			int newWidth = ivWidth;
			int newHeight = (int) Math.Floor((double) currentBitmapHeight *( (double) newWidth / (double) currentBitmapWidth));

			view.FindViewById<ImageView> (Resource.Id.FullUrlImage).SetImageBitmap (imageBitmap);
			view.FindViewById<ImageView> (Resource.Id.FullUrlImage).LayoutParameters.Width = newWidth;
			view.FindViewById<ImageView> (Resource.Id.FullUrlImage).LayoutParameters.Height = newHeight;

			return view;
		}

    	private Bitmap GetImageBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;

			using (var webClient = new WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
				}
			}

			return imageBitmap;
		}

		private byte[] GetByteBitmapFromUrl(string url)
		{
			byte[] imageBytes;
			using (var webClient = new WebClient())
			{
				imageBytes = webClient.DownloadData(url);
			}
			return imageBytes;
		}
//
//		public static Bitmap ResizeImageAndroid (byte[] imageData, float width, float height)
//		{
//			// Load the bitmap
//			Bitmap originalImage = BitmapFactory.DecodeByteArray (imageData, 0, imageData.Length);
//			Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)width, (int)height, false);
//
//			using (MemoryStream ms = new MemoryStream())
//			{
//				resizedImage.Compress (Bitmap.CompressFormat.Jpeg, 100, ms);
//				var imageBitmap = BitmapFactory.DecodeByteArray(ms.ToArray(), 0, ms.ToArray().Length);
//				return imageBitmap;
//			}
//		}
	}
}


