﻿using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using View = Xamarin.Forms.View;
using Android.App;
using Clazloop;
using Clazloop.Droid;

[assembly: ExportRenderer(typeof(CrossVideoPlayerView), typeof(CrossVideoPlayerViewRenderer))]

namespace Clazloop.Droid
{
	/// <summary>
	/// CrossVideoPlayer Renderer for Android.
	/// </summary>
	public class CrossVideoPlayerViewRenderer : ViewRenderer
	{
		/// <summary>
		/// Used for registration with dependency service
		/// </summary>
		public static void Init()
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);
			var crossVideoPlayerView = Element as CrossVideoPlayerView;

			if ((crossVideoPlayerView != null) && (e.OldElement == null))
			{
				var metrics = Resources.DisplayMetrics;
				crossVideoPlayerView.HeightRequest = metrics.WidthPixels/metrics.Density/crossVideoPlayerView.VideoScale;
				var videoView = new VideoView(Context);
				var imgView = new ImageView(Context);
			
				try {
					var uri = Android.Net.Uri.Parse(crossVideoPlayerView.VideoSource);
					var uri1 = Android.Net.Uri.Parse("http://view.vzaar.com/3805950/thumb");
					videoView.SetVideoURI(uri);
					imgView.SetImageURI(uri1);
					var mediaController = new MediaController(Context);
					mediaController.SetMinimumHeight(15);
					mediaController.SetAnchorView(videoView);
					videoView.SetMediaController(mediaController);
					videoView.RequestFocus ();
					videoView.SeekTo(2000);
					videoView.Start();
					SetNativeControl(imgView);
					SetNativeControl(videoView);

					videoView.Touch += (object sender, TouchEventArgs eve) => {
						crossVideoPlayerView.OnClickVideo();
					};

				} catch (System.Exception ex) {
					throw ex;
				}
			}
		}
	}
}