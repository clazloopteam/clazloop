﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Clazloop;
using Clazloop.Droid;
using Android.Graphics;
using Android.Widget;

[assembly: ExportRenderer(typeof(ExtLabel), typeof(ExtLabelRenderer))]

namespace Clazloop.Droid
{
	public class ExtLabelRenderer : LabelRenderer
	{
		private object _xamFormsSender;

		public ExtLabelRenderer()
		{
			_xamFormsSender = null;
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (_xamFormsSender != sender || e.PropertyName == ExtLabel.FontFamilyProperty.PropertyName)
			{
				var font = ((ExtLabel)sender).FontFamily;
				if (!string.IsNullOrEmpty(font))
				{
					if (!font.Contains(".otf")) font += ".otf";
					var typeface = Typeface.CreateFromAsset(Forms.Context.Assets, "fonts/" + font);
					var label = Control as TextView;
					if (label != null) label.Typeface = typeface;
				}
				_xamFormsSender = sender;
			}
		}
	}
}

