﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using DeviceOrientation.Forms.Plugin.Droid;

[assembly: Xamarin.Forms.ExportRenderer(typeof(Telerik.XamarinForms.DataControls.RadListView), typeof(Telerik.XamarinForms.DataControlsRenderer.Android.ListViewRenderer))]
[assembly: Xamarin.Forms.ExportRenderer(typeof(Telerik.XamarinForms.Primitives.RadSideDrawer), typeof(Telerik.XamarinForms.PrimitivesRenderer.Android.SideDrawerRenderer))]

namespace Clazloop.Droid
{
	[Activity (Label = "Clazloop.Droid",Icon = "@drawable/icon",MainLauncher = true,ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,Theme = "@style/MyTheme")]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			global::Xamarin.Forms.Forms.Init (this, bundle);
			DeviceOrientationImplementation.Init();
			var pixelWidth = (int)Resources.DisplayMetrics.WidthPixels; 
			var pixelHeight = (int)Resources.DisplayMetrics.HeightPixels; 
			var screenPixelDensity = (double)Resources.DisplayMetrics.Density;
			App.ScreenHeight = (double)((pixelHeight - 0.5f) / screenPixelDensity);
			App.ScreenWidth = (double)((pixelWidth - 0.5f) / screenPixelDensity);
			System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => { return true; };
			LoadApplication (new App ());
		}

		public override void OnConfigurationChanged(global::Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
			DeviceOrientationImplementation.NotifyOrientationChange(newConfig);
		}
	}
}

